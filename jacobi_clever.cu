#include <cusp/io/matrix_market.h>
#include <cusp/csr_matrix.h>
#include <cusp/print.h>

#include <cuda_runtime.h>
#include <cusparse_v2.h>

#include <cusp/monitor.h>
#include <cusp/krylov/cg.h>
#include <cusp/krylov/bicgstab.h>
#include <cusp/krylov/gmres.h>
#include <cusp/gallery/poisson.h>
#include <cusp/gallery/random.h>

#include <iostream>
#include <stdlib.h>

#include "timer.h"
#include "common.h"


using namespace std;

void generate_cusp_from_csr(csr_matrix h_mat, cusp::csr_matrix<int, float, cusp::host_memory> &h_cusp_mat){
    h_cusp_mat.resize(h_mat.rows, h_mat.rows, h_mat.nnz);

    for (int i = 0; i < h_cusp_mat.num_entries; ++i)
    {
        h_cusp_mat.column_indices[i] = h_mat.colIndex[i];
        h_cusp_mat.values[i] = h_mat.values[i];
    }

    for (int i = 0; i <= h_cusp_mat.num_rows; ++i)
    {
        h_cusp_mat.row_offsets[i] = h_mat.rowPtr[i];
    }

}

int main(int argc, char const *argv[])
{

	int FROM_FILE = 0;

	GpuTimer tridiagTimer;
	GpuTimer cuspZeroTimer;
	GpuTimer cuspHintTimer;

	// load A from disk into a csr_matrix
    cusp::csr_matrix<int, float, cusp::host_memory> h_A;

    // right hand side b
    float* h_b,*d_b;

    // cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

	if(argc > 1)
		FROM_FILE = atoi(argv[1]);

	if(FROM_FILE){
		cusp::io::read_matrix_market_file(h_A, argv[2]);

        // Make the matrix diagonally dominant
        for (int i = 0; i < h_A.num_rows; ++i)
        {
            float sum = 0;
            int position = -1;
            for (int j = h_A.row_offsets[i]; j < h_A.row_offsets[i+1]; ++j)
            {
                sum += abs(h_A.values[j]);
                if(h_A.column_indices[j] == i)
                    position = j;
            }
            if(position != -1){
                h_A.values[position] = sum;
            }
        }

        // Creating b arrray
        h_b = (float*) calloc(h_A.num_rows, sizeof(float));
        cudaMalloc((void**)&d_b, h_A.num_rows*sizeof(float));

        for (int i = 0; i < h_A.num_rows; ++i)
        {
            h_b[i] = 1;
        }

        cudaMemcpy(d_b, h_b, sizeof(float)*h_A.num_rows, cudaMemcpyHostToDevice);
	}else{
        int rows, bCount;
        float nnzPerc, bandPerc;
        int A_MAIN_DIAG_AMPLIFIER, B_AMPLIFIER;

        band_matrix h_band_mat;
        csr_matrix h_sparse_mat, h_qband_mat;

        bCount = 1;
        A_MAIN_DIAG_AMPLIFIER = 1;
        B_AMPLIFIER = 1;

        rows = atoi(argv[2]);
        nnzPerc = atof(argv[3]);
        bandPerc = atof(argv[4]);

        if(argc > 5){
            A_MAIN_DIAG_AMPLIFIER = atoi(argv[5]);
        }

        if(argc > 6){
            B_AMPLIFIER = atoi(argv[6]);
        }


        h_b = (float*) calloc(rows, sizeof(float));
        cudaMalloc<float>(&d_b, rows*sizeof(float));

        if(!create_solver(h_band_mat, h_sparse_mat, h_qband_mat, rows, nnzPerc, bandPerc, h_b, bCount, A_MAIN_DIAG_AMPLIFIER, B_AMPLIFIER))
            return 0;

        cudaMemcpy(d_b, h_b, rows*sizeof(float), cudaMemcpyHostToDevice);

        // Convert host quasiband matrix to cusp quasi band matrix
        generate_cusp_from_csr(h_qband_mat, h_A);

	}

	// Transferring matrix on to device
    cusp::csr_matrix<int, float, cusp::device_memory> d_A(h_A);

    float* h_l, *h_m, *h_u;
    float* d_l, *d_m, *d_u;

    h_l = (float*) calloc(h_A.num_rows, sizeof(float));
    h_m = (float*) calloc(h_A.num_rows, sizeof(float));
    h_u = (float*) calloc(h_A.num_rows, sizeof(float));
    

    // Filling tri diagonal part
    for (int i = 0; i < h_A.num_rows; ++i)
    {
    	for (int j = h_A.row_offsets[i]; j < h_A.row_offsets[i+1]; ++j)
    	{
    		if(i == h_A.column_indices[j]+1){
    			h_l[i] = h_A.values[j];
    		}

    		if(i == h_A.column_indices[j]){
    			h_m[i] = h_A.values[j];
    		}

    		if(i == h_A.column_indices[j]-1){
    			h_u[i] = h_A.values[j];
    		}
    	}
    }


    // Transfering tri diagoanl arrays to device.
    cudaMalloc((void**)&d_l, h_A.num_rows*sizeof(float));
    cudaMalloc((void**)&d_m, h_A.num_rows*sizeof(float));
    cudaMalloc((void**)&d_u, h_A.num_rows*sizeof(float));

    cudaMemcpy(d_l, h_l, sizeof(float)*h_A.num_rows, cudaMemcpyHostToDevice);
    cudaMemcpy(d_m, h_m, sizeof(float)*h_A.num_rows, cudaMemcpyHostToDevice);
    cudaMemcpy(d_u, h_u, sizeof(float)*h_A.num_rows, cudaMemcpyHostToDevice);


    tridiagTimer.Start();
	    cusparseSgtsv(cusparse_handle,
	    			  h_A.num_rows,
	    			  1,
	    			  d_l, d_m, d_u,
	    			  d_b, h_A.num_rows);
	    cudaDeviceSynchronize();
    tridiagTimer.Stop();

    float* h_tr_sol = (float*) calloc(h_A.num_rows, sizeof(float));
    cudaMemcpy(h_tr_sol, d_b, sizeof(float)* h_A.num_rows, cudaMemcpyDeviceToHost);

    // Solving CG using tridiag

	// allocate storage for solution (x) and right hand side (b)
	cusp::array1d<float, cusp::host_memory> cusp_h_x(h_A.num_rows);
	cusp::array1d<float, cusp::host_memory> cusp_h_b(h_A.num_rows);

	for (int i = 0; i < h_A.num_rows; ++i)
	{
		cusp_h_x[i] = h_tr_sol[i];
		cusp_h_b[i] = h_b[i];
	}

    // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<float, cusp::device_memory> cusp_d_clever_x(cusp_h_x);
    cusp::array1d<float, cusp::device_memory> cusp_d_zero_x(h_A.num_rows, 0);

    cusp::array1d<float, cusp::device_memory> cusp_d_clever_b(cusp_h_b);
    cusp::array1d<float, cusp::device_memory> cusp_d_zero_b(cusp_h_b);

    // set stopping criteria:
    //  iteration_limit    = 100
    //  relative_tolerance = 1e-3
    cusp::monitor<float> cleverMonitor(cusp_d_clever_b, 200, 1e-6, 0, true);
    cusp::monitor<float> zeroMonitor(cusp_d_zero_b, 200, 1e-6, 0, true);

    // solve the linear system A * x = b with the Conjugate Gradient method
    cuspZeroTimer.Start();
        cusp::krylov::gmres(d_A, cusp_d_zero_x, cusp_d_zero_b, 200, zeroMonitor);
        //cusp::krylov::bicgstab(d_A, cusp_d_zero_x, cusp_d_zero_b, zeroMonitor);
        cudaDeviceSynchronize();
    cuspZeroTimer.Stop();

    cuspHintTimer.Start();
    	cusp::krylov::gmres(d_A, cusp_d_clever_x, cusp_d_clever_b, 200, cleverMonitor);
        //cusp::krylov::bicgstab(d_A, cusp_d_clever_x, cusp_d_clever_b, cleverMonitor);
    	cudaDeviceSynchronize();
    cuspHintTimer.Stop();

    


    float tridiagTime = tridiagTimer.Elapsed();
    float cuspHintTime = cuspHintTimer.Elapsed();
    float cuspZeroTime = cuspZeroTimer.Elapsed();

    cout << cuspZeroTime/(tridiagTime+cuspHintTime) << endl;
    cout << "My time: " << tridiagTime+cuspHintTime << " (" << tridiagTime << "," << cuspHintTime << ")" << " in iterations " << cleverMonitor.iteration_count() <<  endl;
   	cout << "Bnc tym: " << cuspZeroTime << " in iterations " << zeroMonitor.iteration_count() << endl;


	return 0;
}