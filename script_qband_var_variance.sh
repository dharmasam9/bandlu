
# Synthetic matrix properites
rows=32000
dia_count=3
band_perc=96

num_graphs=3
num_data_points=21
strip_size=5

max_value=100
start_variance=0

result_dir="./results/"
hyphen="-"
mtype="qband"
exp="variancevar"
result=$result_dir$mtype$hyphen$exp$hyphen$rows$hyphen$dia_count

# To get indivudal results
if [ -f "$result" ]
then
	rm $result
else
	touch $result
fi

for ((  i = 0 ;  i < num_graphs;  i++  ))
do
	variance=$start_variance
	for ((  j = 0 ;  j < num_data_points;  j++  ))
	do
		./qband "0" "$rows" "$dia_count" "$band_perc" "1" "$max_value" "$variance"  >> $result
		#./qband "0" "$rows" "$dia_count" "$band_perc" "1" "$max_value" "$variance"
		variance=$((variance+strip_size))
	done
	max_value=$((max_value*10))
done


##### starts with 100,99,95,90....
#for ((  i = 0 ;  i < num_graphs;  i++  ))
#do
#	band_perc=100
#	for ((  j = 0 ;  j < num_data_points;  j++  ))
#	do
#		#./qband "0" "$rows" "$dia_count" "$band_perc" "1" >> $result
#		./qband "0" "$rows" "$dia_count" "$band_perc" "1"
#		#echo "0" "$rows" "$dia_count" "$band_perc" "1"

#		if test $band_perc -eq 100
#		then
#			band_perc=$((band_perc-1))
#		else
#			if [ $band_perc == 99 ]
#			then
#				band_perc=100
#			fi
#			band_perc=$((band_perc-strip_size))
#		fi
#	done
#	dia_count=$((dia_count+2))
#done