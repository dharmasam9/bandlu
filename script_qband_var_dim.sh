
# Synthetic matrix properites
rows=3000
nnz_perc=3
band_perc=100

num_data_points=8
strip_size=3000

result_dir="./results/"
hyphen="-"
mtype="qband"
exp="dimvar"
result=$result_dir$mtype$hyphen$exp$hyphen$nnz_perc$hyphen$band_perc


if [ -f "$result" ]
then
	rm $result
else
	touch $result
fi


for ((  j = 0 ;  j < num_data_points;  j++  ))
	do
		./qband "0" "$rows" "$nnz_perc" "$band_perc" "1" >> $result
		rows=$((rows + strip_size))
		#nnz_perc=$(echo "scale=2; ($nnz_perc+$strip_size)" | bc)
	done