
# Synthetic matrix properites
rows=32000
dia_count=51

num_graphs=1
num_data_points=7
strip_size=3

start_band_perc=$((99 - num_data_points*strip_size + strip_size))
band_perc=$start_band_perc

result_dir="./results/"
hyphen="-"
mtype="qband"
exp="bpercvar"
result=$result_dir$mtype$hyphen$exp$hyphen$rows$hyphen$dia_count

# To get indivudal results
if [ $num_graphs != 1 ]
then
	if [ -f "$result" ]
	then
		rm $result
	else
		touch $result
	fi
fi


for ((  i = 0 ;  i < num_graphs;  i++  ))
do
	band_perc=$start_band_perc
	for ((  j = 0 ;  j < num_data_points;  j++  ))
	do
		./qband "0" "$rows" "$dia_count" "$band_perc" "1" >> $result
		#	./qband "0" "$rows" "$dia_count" "$band_perc" "1"
		#echo "0" "$rows" "$dia_count" "$band_perc" "1"
		band_perc=$((band_perc+strip_size))
	done
	dia_count=$((dia_count+2))
done


##### starts with 100,99,95,90....
#for ((  i = 0 ;  i < num_graphs;  i++  ))
#do
#	band_perc=100
#	for ((  j = 0 ;  j < num_data_points;  j++  ))
#	do
#		#./qband "0" "$rows" "$dia_count" "$band_perc" "1" >> $result
#		./qband "0" "$rows" "$dia_count" "$band_perc" "1"
#		#echo "0" "$rows" "$dia_count" "$band_perc" "1"

#		if test $band_perc -eq 100
#		then
#			band_perc=$((band_perc-1))
#		else
#			if [ $band_perc == 99 ]
#			then
#				band_perc=100
#			fi
#			band_perc=$((band_perc-strip_size))
#		fi
#	done
#	dia_count=$((dia_count+2))
#done