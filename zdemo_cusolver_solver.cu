#include <stdio.h>
#include <iostream>
#include <fstream>

#include <stdlib.h>
#include <sys/time.h>


#include <cuda_runtime.h>

// CUSPARSE
#include <cusparse_v2.h>
#include <cusolverSp.h>

#include "timer.h"


using namespace std;

// Create a synthetic quasi-band matrix in CSR
// Store band part in DIA
// Solve band part
// Perform iterative solver using SPMV and check for tolerance.


int main(int argc, char const *argv[])
{
	GpuTimer band_solver_timer;
	GpuTimer quasiband_solver_timer;

	float benchmark_time = 0;


	// Create handle
	// cusparse handle
	cusolverSpHandle_t cusolver_handle;
	cusolverSpCreate(&cusolver_handle);

	int rows = 5;
	int nnz = 21;

	int h_csrrowPtr[6] = {0,3,7,12,16,19};
	int h_csrColInd[19] = {0,1,2,0,1,2,3,0,1,2,3,4,1,2,3,4,2,3,4};
	float h_csrVal[19] = {19,16,18,15,10,13,18,18,6,8,7,12,22,11,21,22,12,20,15};
	float h_b[5] = {23,8,12,13,7};
	float* h_x = (float*) calloc(rows, sizeof(float));


	int* d_csrrowPtr , *d_csrColInd;
	float* d_csrVal, *d_b, *d_x;


	cudaMalloc((void**)&d_csrrowPtr, sizeof(int) * (rows+1));
	cudaMemcpy(d_csrrowPtr, h_csrrowPtr, sizeof(int)*(rows+1), cudaMemcpyHostToDevice);

	cudaMalloc((void**)&d_csrColInd, sizeof(int) * (nnz));
	cudaMemcpy(d_csrColInd, h_csrColInd, sizeof(int)*(nnz), cudaMemcpyHostToDevice);

	cudaMalloc((void**)&d_csrVal, sizeof(float) * (nnz));
	cudaMemcpy(d_csrVal, h_csrVal, sizeof(float)*(nnz), cudaMemcpyHostToDevice);

	cudaMalloc((void**)&d_b, sizeof(float) * (rows));
	cudaMemcpy(d_b, h_b, sizeof(float)*(rows), cudaMemcpyHostToDevice);

	cudaMalloc((void**)&d_x, sizeof(float) * (rows));
	cudaMemcpy(d_x, h_x, sizeof(float)*(rows), cudaMemcpyHostToDevice);


	// create and setup matrix descriptors A,
	cusparseMatDescr_t cusparse_descrA = 0;
	cusparseCreateMatDescr(&cusparse_descrA);        
	cusparseSetMatType(cusparse_descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrA, CUSPARSE_INDEX_BASE_ZERO);

	quasiband_solver_timer.Start();

	int singularity;
	cusolverSpScsrlsvqr(cusolver_handle, 
		rows,
		nnz, 
		cusparse_descrA, 
		d_csrVal, 
		d_csrrowPtr,
		d_csrColInd, 
		d_b, 0.1,0, d_x, &singularity);

	quasiband_solver_timer.Stop();

	cudaMemcpy(h_x, d_x, sizeof(float)*rows, cudaMemcpyDeviceToHost);

	for (int i = 0; i < rows; ++i)
	{
		cout << h_x[i] << endl;
	}

	cusolverSpDestroy(cusolver_handle);

	cout << "Time taken " << quasiband_solver_timer.Elapsed() << endl;
    
	return 0;
}