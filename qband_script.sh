
# Synthetic matrix properites
rows=3000
bCount=1
nnz_perc=0.5
band_perc=90

num_data_points=5
strip_size=2000

result_dir="./results/"
hyphen="-"
mtype="qband"
exp="sizevar"
result=$result_dir$mtype$hyphen$exp$hyphen$rows$hyphen$bCount


if [ -f "$result" ]
then
	rm $result
else
	touch $result
fi


for ((  j = 0 ;  j < num_data_points;  j++  ))
	do
		./qband "$rows" "$nnz_perc" "$band_perc" >> $result
		rows=$((rows + strip_size))
	done