#include "common.h"

// CUSP related
#include <cusp/monitor.h>
#include <cusp/krylov/gmres.h>


__global__
void Calculate_sub_parts(float* d_values, int* d_colIndex, float* d_b, float* d_sub_parts_values, int rows, int nnz, int itemCount){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < itemCount){
		int elem_id = tid%nnz;
		int solver_id = tid/nnz;

		d_sub_parts_values[tid] = d_b[solver_id*rows + d_colIndex[elem_id]] * d_values[elem_id];
	}
}

__global__
void find_difference(float* d_iter_b, float* d_b, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		d_iter_b[tid] -= d_b[tid];
	}

}


int main(int argc, char const *argv[])
{	
	// Initializing deviceProp variable
	cudaGetDeviceProperties(&deviceProp,0);
	dim3 grids,blocks;

	// Timers and times
	GpuTimer tridiagTimer, cuspHintTimer, cuspZeroTimer;
	float tridiagTime = 0;
	float cuspHintTime = 0;
	float cuspZeroTime = 0;

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

	srand(time(0));
	int FROM_FILE = 0;

	int rows,bCount;
	float nnzPerc, bandPerc;
	band_matrix h_band_mat, d_band_mat;
	csr_matrix h_sparse_mat, d_sparse_mat;
	csr_matrix h_qband_mat, d_qband_mat;

	rows = atoi(argv[1]);
	bCount = atoi(argv[2])
	nnzPerc = atof(argv[3]);
	bandPerc = atof(argv[4]);


	float* h_b,*d_b;
	float* h_init_x;

	h_b = (float*) calloc(rows*bCount, sizeof(float));
	cudaMalloc<float>(&d_b, rows*bCount*sizeof(float));
	h_init_x = (float*) calloc(rows*bCount, sizeof(float));

	if(!create_solver(h_band_mat, h_sparse_mat, h_qband_mat, rows, nnzPerc, bandPerc, h_b, bCount))
		return 0;
	cudaMemcpy(d_b, h_b, rows*bCount*sizeof(float), cudaMemcpyHostToDevice);

	//print_host_csr_matrix(h_qband_mat);
	//print_host_band_matrix(h_band_mat);
	//print_host_csr_matrix(h_sparse_mat);
	//print_host_float_array(h_b,rows);


	// solve for tridiagonal
	float* d_dl,*d_d,*d_du;
	cudaMalloc((void**)&d_dl, rows*sizeof(float));
    cudaMalloc((void**)&d_d, rows*sizeof(float));
    cudaMalloc((void**)&d_du, rows*sizeof(float));

    cudaMemcpy(d_dl, &h_band_mat.data[(h_band_mat.bandwidth-2)*rows], rows*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_d, &h_band_mat.data[(h_band_mat.bandwidth-1)*rows], rows*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_du, &h_band_mat.data[(h_band_mat.bandwidth)*rows], rows*sizeof(float), cudaMemcpyHostToDevice);

    // Getting initial x from the tri diagonal solver
    tridiagTimer.Start();
    cusparseSgtsv(cusparse_handle,
    			  rows,
    			  bCount,
    			  d_dl, d_d, d_du,
    			  d_b, rows);
    cudaDeviceSynchronize();
   	tridiagTimer.Stop();

   	cudaMemcpy(h_init_x, d_b, rows*bCount*sizeof(float), cudaMemcpyDeviceToHost); // Hint x
   	cudaMemcpy(d_b, h_b, rows*bCount*sizeof(float), cudaMemcpyHostToDevice); // restoring d_b

   	// TODO Batched Iterative solver.
   	// Allocate space for subparts.
   	float* d_sub_parts_values;
   	int* h_sub_parts_key,*d_sub_parts_keys;
   	float* h_iter_b, *d_iter_b, *d_output_keys;

   	h_sub_parts_key = (int*) calloc(h_qband_mat.nnz*bCount, sizeof(int));
   	h_iter_b = (float*) calloc(h_qband_mat.rows*bCount, sizeof(float));

   	cudaMalloc((void**)&d_sub_parts_keys, h_qband_mat.nnz * bCount * sizeof(int));
   	cudaMalloc((void**)&d_sub_parts_values, h_qband_mat.nnz * bCount * sizeof(float));
   	cudaMalloc((void**)&d_iter_b, h_qband_mat.rows * bCount * sizeof(float));
   	cudaMalloc((void**)&d_output_keys, h_qband_mat.rows * bCount * sizeof(float));


   	for (int i = 0; i < h_qband_mat.rows; ++i)
   	{
   		for (int j = h_qband_mat.rowPtr[i]; j < h_qband_mat.rowPtr[i+1]; ++j)
   		{
   			for (int k = 0; k < bCount; ++k)
   			{
   				h_sub_parts_key[k*rows + j] = k*rows + i;
   			}
   		}
   	}

   	// Calculate subparts
   	// nnz*bCount threads
   	find_dimensions_of_blocks_and_threads(h_qband_mat.nnz*bCount, grids, blocks);
   	Calculate_sub_parts<<<grids,blocks>>>(h_qband_mat.values, h_qband_mat.colIndex, d_b, d_sub_parts_values, h_qband_mat.rows, h_qband_mat.nnz, h_qband_mat.nnz*bCount);

   	// Allocate space for iterative solution
   	// Reduce them to find the iterative solution
   	thrust::reduce_by_key(d_sub_parts_keys, d_sub_parts_keys+(h_qband_mat.nnz*bCount), d_sub_parts_values, 
   		d_output_keys, d_iter_b);
   	// Find the difference (b_new - b_old)
   	find_dimensions_of_blocks_and_threads(rows*bCount, grids, blocks);
   	find_difference<<<grids,blocks>>>(d_iter_b, d_b, rows*bCount);

   	// Check which b's are solved and clean data.
   	// Repeat until all b's are solved


	
	cusparseDestroy(cusparse_handle);



}
