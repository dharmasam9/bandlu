#include "common.h"

// CUSP related
#include <cusp/monitor.h>
#include <cusp/krylov/gmres.h>


/*
Input : quasi-band matrix

Approach : Solves the band part of the matrix
		   and uses that as the initial solution.
*/


int main(int argc, char const *argv[])
{	
	// Initializing deviceProp variable
	cudaGetDeviceProperties(&deviceProp,0);
	dim3 grids,blocks;

	// Timers and times
	GpuTimer bandTimer, cuspHintTimer, cuspZeroTimer;
	float bandTime = 0;
	float cuspHintTime = 0;
	float cuspZeroTime = 0;

	// Stopping criteria from iterative kernels.
    int  iteration_limit    = 500;
    float  relative_tolerance = 1e-6;
    int restart = 500;

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

	srand(time(0));
	int FROM_FILE = 0;

	int rows,bandwidth,dia_count;
	float nnzPerc, bandPerc;
	band_matrix h_band_mat, d_band_mat;
	csr_matrix h_band_csr_mat, d_band_csr_mat;
	csr_matrix h_sparse_mat, d_sparse_mat;
	csr_matrix h_qband_mat, d_qband_mat;

	float* h_b,*d_b;
	float* h_init_x;

	h_b = (float*) calloc(rows, sizeof(float));
	cudaMalloc<float>(&d_b, rows*sizeof(float));
	h_init_x = (float*) calloc(rows, sizeof(float));

	rows = atoi(argv[1]);
	nnzPerc = atof(argv[2]);
	bandPerc = atof(argv[3]);

	if(!create_solver(h_band_mat, h_sparse_mat, h_qband_mat, rows, nnzPerc, bandPerc, h_b))
		return 0;
	cudaMemcpy(d_b, h_b, rows*sizeof(float), cudaMemcpyHostToDevice);

	// Converting band matrix in dia format to csr format.
	create_csr_from_band(h_band_mat, h_band_csr_mat);

	//print_host_csr_matrix(h_qband_mat);
	//print_host_band_matrix(h_band_mat);
	//print_host_csr_matrix(h_sparse_mat);
	//print_host_float_array(h_b,rows);
	//print_host_csr_matrix(h_band_csr_mat);
	

	// TODO solve for band
	cusp::csr_matrix<int,float,cusp::host_memory> h_A_band(rows, rows, h_band_csr_mat.nnz);
	// Transferring to cusp host csr structure
    for (int i = 0; i < h_band_csr_mat.nnz; ++i)
    {
    	h_A_band.column_indices[i] = h_band_csr_mat.colIndex[i];
    	h_A_band.values[i] = h_band_csr_mat.values[i];
    }

    for (int i = 0; i < rows+1; ++i)
    {
    	h_A_band.row_offsets[i] = h_band_csr_mat.rowPtr[i];
    }

    cusp::csr_matrix<int,float,cusp::device_memory> d_A_band = h_A_band;

    // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<float, cusp::host_memory> x(rows);
    cusp::array1d<float, cusp::host_memory> b(rows);

    for (int i = 0; i < rows; ++i)
    {
    	x[i] = h_init_x[i];
    	b[i] = h_b[i];
    }


    // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<float, cusp::device_memory> d_band_x = x;
    cusp::array1d<float, cusp::device_memory> d_b_init = b;

    cusp::monitor<float> monitorInit(d_b_init, iteration_limit, relative_tolerance);
    

    // solve the linear system A * x = b with the GMRES
    bandTimer.Start();
    	cusp::krylov::gmres(d_A_band, d_band_x, d_b_init,restart, monitorInit);
    	cudaDeviceSynchronize();
    bandTimer.Stop();


	// Solve it using CUSP
    cusp::csr_matrix<int,float,cusp::host_memory> h_A(rows, rows, h_qband_mat.nnz);

    // Transferring to cusp host csr structure
    for (int i = 0; i < h_qband_mat.nnz; ++i)
    {
    	h_A.column_indices[i] = h_qband_mat.colIndex[i];
    	h_A.values[i] = h_qband_mat.values[i];
    }

    for (int i = 0; i < rows+1; ++i)
    {
    	h_A.row_offsets[i] = h_qband_mat.rowPtr[i];
    }

    cusp::csr_matrix<int,float,cusp::device_memory> d_A = h_A;

    // print_host_csr_matrix(h_qband_mat);
    //cusp::print(d_A);
   
   // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<float, cusp::device_memory> d_b_hint = b;
    cusp::array1d<float, cusp::device_memory> d_b_zero = b;


    
    cusp::monitor<float> monitorHint(d_b_hint, iteration_limit, relative_tolerance);

    // solve the linear system A * x = b with the GMRES
    cuspHintTimer.Start();
    	cusp::krylov::gmres(d_A, d_band_x, d_b_hint,restart, monitorHint);
    	cudaDeviceSynchronize();
    cuspHintTimer.Stop();

    

    // x with zero as starting point
	cusp::array1d<float, cusp::device_memory> d_x_zero(rows, float(0));
	cusp::monitor<float> monitorZero(d_b_zero, iteration_limit, relative_tolerance);

	cuspZeroTimer.Start();
		cusp::krylov::gmres(d_A, d_x_zero, d_b_zero,restart, monitorZero);
		cudaDeviceSynchronize();
	cuspZeroTimer.Stop();

	bandTime = bandTimer.Elapsed();
	cuspHintTime = cuspHintTimer.Elapsed();
	cuspZeroTime = cuspZeroTimer.Elapsed();

	int ANALYSIS = 0;

	if(monitorInit.converged()){
		if(!ANALYSIS) cout << "Band converged under " << monitorInit.iteration_count() << endl;
	}else{
		if(!ANALYSIS) cout << "Band did not converge" << endl;
	}

	if(monitorHint.converged()){
		if(!ANALYSIS) cout << "Hint converged under " << monitorHint.iteration_count() << endl;
	}else{
		if(!ANALYSIS) cout << "Hint did not converge" << endl;
	}

	if(monitorZero.converged()){
		if(!ANALYSIS) cout << "Zero converged under " << monitorZero.iteration_count() << endl;
	}else{
		if(!ANALYSIS) cout << "Zero did not converge" << endl;
	}

	if(ANALYSIS){
		cout << cuspZeroTime/(bandTime+cuspHintTime) << " " << rows << " " << nnzPerc << " " << h_qband_mat.nnz << " " << h_band_mat.bandwidth << " ";
		cout << monitorZero.iteration_count() << " " << cuspZeroTime << " " << monitorHint.iteration_count() << " " << bandTime+cuspHintTime << " " << bandTime << " " << cuspHintTime << endl;
	}else{
		cout << "My time: " << bandTime+cuspHintTime << " (" << bandTime << "," << cuspHintTime << ")" << endl;
   		cout << "Bnc tym: " << cuspZeroTime << endl;	
	}


	cusparseDestroy(cusparse_handle);



}
