#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <cuda_runtime.h>
#include "cublas_v2.h"

#include <iostream>

using namespace std;

__global__
void modify_kernel(float** devPtr){
    int tid = threadIdx.x;
    devPtr[0][0] = 3;
}

int main(int argc, char const *argv[])
{
    cublasHandle_t handle;
    cublasStatus_t status;

    cublasCreate(&handle);

    int rows = 5;



    float h_A[25] = { 5, 3, 0, 0, 0,
                      7, 4, 5, 0, 0,
                      0, 2, 7, 3, 0,
                      0, 0, 5, 4, 3,
                      0, 0, 0, 6, 5
                            };
    int batchSize = 1;

    float* d_A, *d_invA;
    cudaMalloc<float>(&d_A, sizeof(float)* rows * rows);
    cudaMalloc<float>(&d_invA, sizeof(float)* rows * rows);

    cudaMemcpy(d_A, h_A, sizeof(float) * rows * rows, cudaMemcpyHostToDevice);

    float** h_Array = (float**) calloc(1, sizeof(float*));
    float** h_Crray = (float**) calloc(1, sizeof(float*));
    h_Array[0] = d_A;
    h_Crray[0] = d_invA;

    float** d_Array;
    float** d_Crray;

    cudaMalloc<float*>(&d_Array, sizeof(float*));
    cudaMalloc<float*>(&d_Crray, sizeof(float*));

    cudaMemcpy(d_Array, h_Array, sizeof(float*), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Crray, h_Crray, sizeof(float*), cudaMemcpyHostToDevice);


    // Allocate d_Carray

    int *P, *INFO;

    cudaMalloc<int>(&P,rows * batchSize * sizeof(int));
    cudaMalloc<int>(&INFO,batchSize * sizeof(int));

    cublasSgetrfBatched(handle, 
                        rows,
                        d_Array,
                        rows,
                        P,
                        INFO,
                        batchSize);

    const float** d_Array_const = (const float**)d_Array;
    const int* P_const = P;

    cublasSgetriBatched(handle,
                        rows,
                        d_Array_const,
                        rows,
                        P_const,
                        d_Crray,
                        rows,
                        INFO,
                        batchSize);


    


    float test2[25];
    cudaMemcpy(test2, d_invA, 25 * sizeof(float), cudaMemcpyDeviceToHost);

    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < rows; ++j)
        {
                cout << test2[i*rows + j] << " ";
        }
        cout << endl;
    }



    return 0;
    /*

        cublasStatus_t cublasSgetrfBatched(cublasHandle_t handle,
                                   int n, 
                                   float *Aarray[],
                                   int lda, 
                                   int *PivotArray,
                                   int *infoArray,
                                   int batchSize);

    
    float* src_d;
    float* dst_h, *dst_d;

    dst_h = (float*) calloc(rows*rows, sizeof(float));

    cudaMalloc((void**)&src_d, sizeof(float) * rows * rows);
    cudaMemcpy(src_d, h_src, sizeof(float) * rows * rows, cudaMemcpyHostToDevice);

    cudaMalloc((void**)&dst_d, sizeof(float) * rows * rows);

    int batchSize = 1;
    int lda = rows;

    int *P, *INFO;

    cudaMalloc<int>(&P,rows * batchSize * sizeof(int));
    cudaMalloc<int>(&INFO,batchSize * sizeof(int));

    float *A[] = { src_d };
    float** A_d;
    cudaMalloc<float*>(&A_d,sizeof(A));
    cudaMemcpy(A_d,A,sizeof(A),cudaMemcpyHostToDevice);

    cublasSgetrfBatched(handle,rows,A_d,lda,P,INFO,batchSize);

    int INFOh = 0;
    cudaMemcpy(&INFOh,INFO,sizeof(int),cudaMemcpyDeviceToHost);


    float* C[] = { dst_d };
    float** C_d;
    cudaMalloc<float*>(&C_d,sizeof(C));
    cudaMemcpy(C_d,C,sizeof(C),cudaMemcpyHostToDevice);

    cublasSgetriBatched(handle,
        rows,
        A_d,
        lda,
        P,
        C_d,
        lda,
        INFO,
        batchSize);


    /*
    status = cublasSgetriBatched(cublasHandle_t handle,
                                   int n,
                                   float *Aarray[],
                                   int lda,
                                   int *PivotArray,
                                   float *Carray[],
                                   int ldc,
                                   int *infoArray,
                                   int batchSize);
    */
    cublasDestroy(handle);



    return 0;
}