#include "common.h"

int main(int argc, char const *argv[])
{	
	// Initializing deviceProp variable
	cudaGetDeviceProperties(&deviceProp,0);
	dim3 grids,blocks;

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

    // cublas handle
    cublasHandle_t cublas_handle;
    cublasCreate(&cublas_handle);

	srand(time(0));
	int FROM_FILE = 0;

	GpuTimer lu_decomp_timer;
	GpuTimer upper_dets_timer, lower_dets_timer, inverse_timer;
	
	float lu_decomp_time = 0;
	float upper_dets_time = 0 , lower_dets_time = 0, inverse_time = 0;
	float cublas_band_inv_time = 0;

	int rows,bandwidth,dia_count;
	band_matrix h_band_mat, d_band_mat;
	csr_matrix h_sparse_mat, d_sparse_mat;
	csr_matrix h_qband_mat, d_qband_mat;

	if(argc > 1){
		FROM_FILE = atoi(argv[1]);
	}

	if(FROM_FILE){
		ifstream myfile;
		myfile.open("input");
		myfile >> rows >> bandwidth;
		dia_count = 2*bandwidth - 1;

		// SETTING UP BAND MATRIX
		h_band_mat.rows = rows;
		h_band_mat.cols = rows;
		h_band_mat.bandwidth = bandwidth;

		h_band_mat.data = (float*) calloc(rows*dia_count, sizeof(float));

		// READING BAND MATRIX
		int offset;
		float elem;
		int band_nnz_count = 0;
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < rows; ++j)
			{
				offset = j-i;
				myfile >> elem;
				if(offset > -1*(bandwidth) && offset < bandwidth){
					h_band_mat.data[(offset+bandwidth-1)*h_band_mat.rows + i] = elem;
					if(elem != 0)  band_nnz_count++;
				}
			}
		}
		h_band_mat.nnz = band_nnz_count;

		// SETTING UP SPARSE MATRIX
		int sparse_nnz_count;
		myfile >> sparse_nnz_count;

		h_sparse_mat.rows = rows;
		h_sparse_mat.cols = rows;
		h_sparse_mat.nnz = sparse_nnz_count;

		h_sparse_mat.rowPtr = (int*) calloc(rows+1, sizeof(int));
		h_sparse_mat.colIndex = (int*) calloc(sparse_nnz_count, sizeof(int));
		h_sparse_mat.values = (float*) calloc(sparse_nnz_count, sizeof(float));

		int index = 0;
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < rows; ++j)
			{
				myfile >> elem;
				if(elem != 0){
					h_sparse_mat.colIndex[index] = j;
					h_sparse_mat.values[index] = elem;
					h_sparse_mat.rowPtr[i]++;
					index++;
				}
			}
		}

		// exclusive prefix sum on rowcount
		int temp;
		int sum = 0;
		for (int i = 0; i < rows+1; ++i)
		{
			temp = h_sparse_mat.rowPtr[i];
			h_sparse_mat.rowPtr[i] = sum;
			sum += temp;
		}

		csr_matrix band_as_csr_mat;
		create_csr_from_band(h_band_mat, band_as_csr_mat);
		merge_two_csr_parts(band_as_csr_mat, h_sparse_mat, h_qband_mat);

	}else{
		rows = 7;
		float nnzPerc = 70;
		float bandPerc = 90;

		if(argc > 2){
			rows = atoi(argv[2]);
		}
		if(argc > 3){
			nnzPerc = atof(argv[3]);
		}
		if(argc > 4){
			bandPerc = atof(argv[4]);
		}

		// Just to use create_solver h_b is passed other wise it is not needed
		float* h_b = (float*) calloc(rows, sizeof(float));
		create_solver(h_band_mat, h_sparse_mat, h_qband_mat, rows, nnzPerc, bandPerc, h_b);
		bandwidth = h_band_mat.bandwidth;
		dia_count = 2*h_band_mat.bandwidth - 1;

	}

	// print_host_band_matrix(h_band_mat);
	// print_host_csr_matrix(h_sparse_mat);
	// print_host_csr_matrix(h_qband_mat);

	// TRANSFERRING DATA TO DEVICE
	create_device_band_matrix(h_band_mat, d_band_mat);
	create_device_csr_matrix(h_sparse_mat, d_sparse_mat);
	create_device_csr_matrix(h_qband_mat, d_qband_mat);

	// Get the diaogonal vector of the matrix.
	float* h_dia_vec,*d_dia_vec;
	h_dia_vec = (float*) calloc(h_band_mat.rows, sizeof(float));
	memcpy(h_dia_vec, &h_band_mat.data[(h_band_mat.bandwidth-1)*h_band_mat.rows], h_band_mat.rows*sizeof(float));

	cudaMalloc((void**)&d_dia_vec, sizeof(float) * (rows));
	cudaMemcpy(d_dia_vec, h_dia_vec, sizeof(float)*(rows), cudaMemcpyHostToDevice);

	// Prefix multiplicaiton of dia_vec of U matrix
	float* h_prefix_U_dia_vec, *d_prefix_U_dia_vec;
	h_prefix_U_dia_vec = (float*) calloc(rows+1, sizeof(float));
	cudaMalloc((void**)&d_prefix_U_dia_vec, sizeof(float) * (rows+1));
	cudaMemset(d_prefix_U_dia_vec,0, (rows+1)*sizeof(float));

	
	// ************************************ PARTITION BAND TO LU *********************************************
	float* d_lu;
	cudaMalloc((void**)&d_lu, sizeof(float) * (dia_count*d_band_mat.rows));
	cudaMemset(d_lu,0, sizeof(float) * (dia_count*d_band_mat.rows));

	find_dimensions_of_blocks_and_threads(h_band_mat.bandwidth, grids, blocks);
	dim3 grids_temp, blocks_temp;
	find_dimensions_of_blocks_and_threads(h_band_mat.bandwidth-1, grids_temp, blocks_temp);

	lu_decomp_timer.Start();
		
		// Perform the computation.
		for (int i = 0; i < h_band_mat.rows-1; ++i)
		{	
			compute_upper_triangle_row<<<grids,blocks>>>(i, h_band_mat.rows, h_band_mat.bandwidth, d_band_mat.data, d_lu);
			compute_lower_triangle_column<<<grids_temp,blocks_temp>>>(i, h_band_mat.rows, h_band_mat.bandwidth, d_band_mat.data, d_lu);
		}
		compute_upper_triangle_row<<<grids,blocks>>>(h_band_mat.rows-1, h_band_mat.rows, h_band_mat.bandwidth, d_band_mat.data, d_lu);
		cudaDeviceSynchronize();
	lu_decomp_timer.Stop();

	float* lu = (float*) calloc(dia_count*h_band_mat.rows, sizeof(float));
	cudaMemcpy(lu, d_lu, sizeof(float)*(dia_count*h_band_mat.rows), cudaMemcpyDeviceToHost);

	// Check whether the result is true.
	// print_host_band_matrix(h_band_mat);
	//naive_check_lu(lu, h_band_mat);

	band_matrix h_lu_mat;
	h_lu_mat.rows = h_band_mat.rows;
	h_lu_mat.cols = h_band_mat.cols;
	h_lu_mat.bandwidth = h_band_mat.bandwidth;
	h_lu_mat.nnz = (2*h_band_mat.bandwidth-1)* h_band_mat.rows - (h_band_mat.bandwidth-1)*h_band_mat.bandwidth;
	h_lu_mat.data = lu;

	// print_host_band_matrix(h_lu_mat); //For seeing the actual values of L and U

	// Preparing prefix multiplication of U's main diagonal
	cudaMemcpy(h_prefix_U_dia_vec, &d_lu[(bandwidth-1)*rows], rows*sizeof(float), cudaMemcpyDeviceToHost);

	float temp;
	float mul = 1;
	for (int i = 0; i < rows+1; ++i)
	{	
		temp = h_prefix_U_dia_vec[i];
		h_prefix_U_dia_vec[i] = mul;
		mul *= temp;
	}
	// print_host_float_array(h_prefix_U_dia_vec, rows+1);

	cudaMemcpy(d_prefix_U_dia_vec, h_prefix_U_dia_vec, (rows+1)*sizeof(float), cudaMemcpyHostToDevice);
	

	float* h_dets, *d_dets;
	h_dets = (float*) calloc(rows*rows, sizeof(float));

	// Set det(i,i) to value one
	for (int i = 0; i < rows; ++i)
			h_dets[i*rows+i] = 1;

	cudaMalloc((void**)&d_dets, sizeof(float) * rows * rows);
	cudaMemset(d_dets,0, sizeof(float) * rows * rows);
	
	upper_dets_timer.Start();
		//  Compute upper derivate matrix
		for (int dia_index = 0; dia_index < rows; ++dia_index)
		{
			find_dimensions_of_blocks_and_threads(rows-dia_index, grids, blocks);
			compute_upper_dets<<<grids,blocks>>>(rows, bandwidth, dia_count, dia_index , d_lu, d_dia_vec, d_dets);
			cudaDeviceSynchronize();
		}
	upper_dets_timer.Stop();

	lower_dets_timer.Start();
		// Compute lower derivate matrix
		for (int dia_index = 0; dia_index < rows; ++dia_index)
		{
			find_dimensions_of_blocks_and_threads(rows-dia_index, grids, blocks);
			compute_lower_dets<<<grids,blocks>>>(rows, bandwidth, dia_count, -1*dia_index , d_lu, d_dia_vec, d_dets);
			cudaDeviceSynchronize();
		}
	lower_dets_timer.Stop();
	// print_device_block_matrix(d_dets,rows);

	find_dimensions_of_blocks_and_threads(rows*rows, grids, blocks);
	inverse_timer.Start();
		find_inverse_of_L_and_U<<<grids,blocks>>>(rows, d_dets, d_prefix_U_dia_vec);
		cudaDeviceSynchronize();
	inverse_timer.Stop();

	cudaMemcpy(h_dets, d_dets, sizeof(float)*rows*rows, cudaMemcpyDeviceToHost);

	// print_host_block_matrix(h_dets,rows);

	// Computing inverse of band matrix using cublas routine.
	block_matrix h_block_band_mat, d_block_band_mat;
	float* h_band_inv, *d_band_inv;
	h_band_inv = (float*) calloc(rows*rows, sizeof(float));
	cudaMalloc<float>(&d_band_inv, sizeof(float)*rows*rows);


	convert_host_band_to_host_block(h_band_mat, h_block_band_mat);
	create_device_block_matrix(h_block_band_mat, d_block_band_mat);

	find_inverse_using_cublas(cublas_handle, rows, d_block_band_mat.data, d_band_inv, cublas_band_inv_time);
	cudaMemcpy(h_band_inv, d_band_inv, sizeof(float)* rows * rows, cudaMemcpyDeviceToHost );

	//print_device_block_matrix(d_block_band_mat.data, d_block_band_mat.rows);
	//print_host_block_matrix(h_band_inv, rows);

	// TODO My qband inverse using inv(Aband) as starting point.

	/*
	// Inverse of the quasi band matrix using cublas routine
	block_matrix d_block_qband_mat, h_block_qband_mat;
	float* d_qband_inv;
	cudaMalloc<float>(&d_qband_inv, sizeof(float)*rows*rows);

	convert_host_csr_to_host_block(h_qband_mat, h_block_qband_mat);
	create_device_block_matrix(h_block_qband_mat, d_block_qband_mat);
	float cublas_qband_inv_time = 0;

	find_inverse_using_cublas(cublas_handle, rows, d_block_qband_mat.data, d_qband_inv, cublas_qband_inv_time);

	cout << "inv(QB) tym " << cublas_qband_inv_time << endl;
	*/
	cout << "inv(B) time " << cublas_band_inv_time << endl;

	lu_decomp_time = lu_decomp_timer.Elapsed();
	upper_dets_time = upper_dets_timer.Elapsed();
	lower_dets_time = lower_dets_timer.Elapsed();
	inverse_time = inverse_timer.Elapsed();

	cout << "My Band tym " << lu_decomp_time + lower_dets_time + upper_dets_time + inverse_time << endl;
	cout << lu_decomp_time << " " << lower_dets_time << " " << upper_dets_time << " " << inverse_time << endl;

	return 0;
}










