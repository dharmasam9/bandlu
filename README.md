**Contains following codes**

* Iterative quasi-tridiagonal solver using CUDA.
* GPU implementation of [paper](http://faculty.nps.edu/pstanica/research/2013JCAM_BandedMatr.pdf) titled "The inverse of Banded Matrices"