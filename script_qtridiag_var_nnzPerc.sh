
# Synthetic matrix properites
rows=10000
bCount=1
non_band_factor=1

num_data_points=20
stride=0.5

result_dir="./results/"
hyphen="-"
mtype="tridiag"
exp="nonbandvar"
result=$result_dir$mtype$hyphen$exp$hyphen$rows


if [ -f "$result" ]
then
	rm $result
else
	touch $result
fi


for ((  j = 0 ;  j < num_data_points;  j++  ))
	do
		./tridiag "$rows" "$bCount" "$non_band_factor" "1" >> $result
		non_band_factor=$(echo "scale=2; ($non_band_factor+$stride)" | bc)
		#echo $non_band_factor
	done