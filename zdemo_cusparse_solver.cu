#include <iostream>

#include <cuda_runtime.h>
#include <cusparse_v2.h>

#include "common.h"

using namespace std;

int main(int argc, char const *argv[])
{
	int rows = 5;
	int nnz = 19;

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

    cusparseStatus_t status;

    
	int h_csrrowPtr[6] = {0,3,7,12,16,19};
	int h_csrColInd[19] = {0,1,2,0,1,2,3,0,1,2,3,4,1,2,3,4,2,3,4};
	float h_csrVal[19] = {13,2,15,8,7,5,6,23,7,12,5,6,21,3,9,8,7,9,25};
	
	/*
	int h_csrrowPtr[6] = {0,1,3,6,9,12};
	int h_csrColInd[12] = {0,0,1,0,1,2,1,2,3,2,3,4};
	float h_csrVal[12] = {19,8,7,23,7,12,21,3,9,8,9,25};
	*/
	float h_b[5] = {23,8,12,13,7};
	float* h_x = (float*) calloc(rows, sizeof(float));

	csr_matrix csr_mat;
	csr_mat.rows = rows; csr_mat.cols = rows;
	csr_mat.rowPtr = h_csrrowPtr;
	csr_mat.colIndex = h_csrColInd;
	csr_mat.values = h_csrVal;

	print_host_csr_matrix(csr_mat);


	int* d_csrrowPtr , *d_csrColInd;
	float* d_csrVal, *d_b, *d_x;


	cudaMalloc((void**)&d_csrrowPtr, sizeof(int) * (rows+1));
	cudaMemcpy(d_csrrowPtr, h_csrrowPtr, sizeof(int)*(rows+1), cudaMemcpyHostToDevice);

	cudaMalloc((void**)&d_csrColInd, sizeof(int) * (nnz));
	cudaMemcpy(d_csrColInd, h_csrColInd, sizeof(int)*(nnz), cudaMemcpyHostToDevice);

	cudaMalloc((void**)&d_csrVal, sizeof(float) * (nnz));
	cudaMemcpy(d_csrVal, h_csrVal, sizeof(float)*(nnz), cudaMemcpyHostToDevice);

	cudaMalloc((void**)&d_b, sizeof(float) * (rows));
	cudaMemcpy(d_b, h_b, sizeof(float)*(rows), cudaMemcpyHostToDevice);

	cudaMalloc((void**)&d_x, sizeof(float) * (rows));
	cudaMemcpy(d_x, h_x, sizeof(float)*(rows), cudaMemcpyHostToDevice);

	// create and setup matrix descriptors A,
	cusparseMatDescr_t cusparse_descrA = 0;
	cusparseCreateMatDescr(&cusparse_descrA);        
	cusparseSetMatType(cusparse_descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrA, CUSPARSE_INDEX_BASE_ZERO);

	cusparseSolveAnalysisInfo_t info;
	status = cusparseCreateSolveAnalysisInfo(&info);

	cusparseScsrsv_analysis(cusparse_handle,
							CUSPARSE_OPERATION_NON_TRANSPOSE,
							rows,
							nnz,
							cusparse_descrA,
							d_csrVal,
							d_csrrowPtr,
							d_csrColInd,
							info);

	float alpha = 1;
	cusparseScsrsv_solve(cusparse_handle, 
						 CUSPARSE_OPERATION_NON_TRANSPOSE,
						 rows, &alpha,
						 cusparse_descrA,
						 d_csrVal,
						 d_csrrowPtr, d_csrColInd,
						 info, d_b, d_x);

	cudaDeviceSynchronize();

	cusparseDestroySolveAnalysisInfo(info);

	cudaMemcpy(h_x, d_x, rows*sizeof(float), cudaMemcpyDeviceToHost);

	for (int i = 0; i < rows; ++i)
	{
		cout << h_x[i] << endl;
	}

	
	cusparseDestroy(cusparse_handle);
}