/*
Ax=b where A is quasi-band matrix.
Comparing two iterative methods to solve x.
1: Start with zero vector as the base iteration.
2: Start with Tr(A)x=b as the base solution.

Compare number of iterations and running times.

// Things to meddle
* Generating values of A 
* Generating values of main diagonal of A
* Generating values of B
*/
#include "common.h"

// CUSP related
#include <cusp/monitor.h>
#include <cusp/krylov/gmres.h>



int main(int argc, char const *argv[])
{	
	// Initializing deviceProp variable
	cudaGetDeviceProperties(&deviceProp,0);
	dim3 grids,blocks;

	// Timers and times
	GpuTimer tridiagTimer, cuspHintTimer, cuspZeroTimer;
	float tridiagTime = 0;
	float cuspHintTime = 0;
	float cuspZeroTime = 0;

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

	srand(time(0));
	int FROM_FILE = 0;

	int rows,bandwidth,dia_count, bCount;
	float nnzPerc, bandPerc;
	band_matrix h_band_mat, d_band_mat;
	csr_matrix h_sparse_mat, d_sparse_mat;
	csr_matrix h_qband_mat, d_qband_mat;

	rows = atoi(argv[1]);
	nnzPerc = atof(argv[2]);
	bandPerc = atof(argv[3]);
	bCount = 1;

	int A_MAIN_DIAG_AMPLIFIER = 1;
	int B_AMPLIFIER = 1;

	if(argc > 4){
		A_MAIN_DIAG_AMPLIFIER = atoi(argv[4]);
	}

	if(argc > 5){
		B_AMPLIFIER = atoi(argv[5]);
	}


	float* h_b,*d_b;
	float* h_init_x;

	h_b = (float*) calloc(rows, sizeof(float));
	cudaMalloc<float>(&d_b, rows*sizeof(float));
	h_init_x = (float*) calloc(rows, sizeof(float));

	if(!create_solver(h_band_mat, h_sparse_mat, h_qband_mat, rows, nnzPerc, bandPerc, h_b, bCount, A_MAIN_DIAG_AMPLIFIER, B_AMPLIFIER))
		return 0;
	cudaMemcpy(d_b, h_b, rows*sizeof(float), cudaMemcpyHostToDevice);

	//print_host_csr_matrix(h_qband_mat);
	//print_host_band_matrix(h_band_mat);
	//print_host_csr_matrix(h_sparse_mat);
	//print_host_float_array(h_b,rows);


	// TODO solve for tridiagonal
	float* d_dl,*d_d,*d_du;
	cudaMalloc((void**)&d_dl, rows*sizeof(float));
    cudaMalloc((void**)&d_d, rows*sizeof(float));
    cudaMalloc((void**)&d_du, rows*sizeof(float));

    cudaMemcpy(d_dl, &h_band_mat.data[(h_band_mat.bandwidth-2)*rows], rows*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_d, &h_band_mat.data[(h_band_mat.bandwidth-1)*rows], rows*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_du, &h_band_mat.data[(h_band_mat.bandwidth)*rows], rows*sizeof(float), cudaMemcpyHostToDevice);

    // Getting initial x from the tri diagonal solver
    tridiagTimer.Start();
    cusparseSgtsv(cusparse_handle,
    			  rows,
    			  bCount,
    			  d_dl, d_d, d_du,
    			  d_b, rows);
    cudaDeviceSynchronize();
   	tridiagTimer.Stop();

   	cudaMemcpy(h_init_x, d_b, rows*sizeof(float), cudaMemcpyDeviceToHost);

	// Solve it using CUSP
    cusp::csr_matrix<int,float,cusp::host_memory> h_A(rows, rows, h_qband_mat.nnz);

    // Transferring to cusp host csr structure
    for (int i = 0; i < h_qband_mat.nnz; ++i)
    {
    	h_A.column_indices[i] = h_qband_mat.colIndex[i];
    	h_A.values[i] = h_qband_mat.values[i];
    }

    for (int i = 0; i < rows+1; ++i)
    {
    	h_A.row_offsets[i] = h_qband_mat.rowPtr[i];
    }

    cusp::csr_matrix<int,float,cusp::device_memory> d_A = h_A;

    // print_host_csr_matrix(h_qband_mat);
    //cusp::print(d_A);
    

    // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<float, cusp::host_memory> x(rows);
    cusp::array1d<float, cusp::host_memory> b(rows);

    for (int i = 0; i < rows; ++i)
    {
    	x[i] = h_init_x[i];
    	b[i] = h_b[i];
    }

   // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<float, cusp::device_memory> d_x = x;
    cusp::array1d<float, cusp::device_memory> d_b_hint = b;
    cusp::array1d<float, cusp::device_memory> d_b_zero = b;


    // set stopping criteria:
    int  iteration_limit    = 500;
    float  relative_tolerance = 1e-6;

    cusp::monitor<float> monitorHint(d_b_hint, iteration_limit, relative_tolerance);
    int restart = 500;

    // solve the linear system A * x = b with the GMRES
    cuspHintTimer.Start();
    	cusp::krylov::gmres(d_A, d_x, d_b_hint,restart, monitorHint);
    	cudaDeviceSynchronize();
    cuspHintTimer.Stop();

    

    // x with zero as starting point
	cusp::array1d<float, cusp::device_memory> d_x_zero(rows, float(0));
	cusp::monitor<float> monitorZero(d_b_zero, iteration_limit, relative_tolerance);

	cuspZeroTimer.Start();
		cusp::krylov::gmres(d_A, d_x_zero, d_b_zero,restart, monitorZero);
		cudaDeviceSynchronize();
	cuspZeroTimer.Stop();

	tridiagTime = tridiagTimer.Elapsed();
	cuspHintTime = cuspHintTimer.Elapsed();
	cuspZeroTime = cuspZeroTimer.Elapsed();

	int ANALYSIS = 1;

	if(monitorHint.converged()){
		if(!ANALYSIS) cout << "Hint converged under " << monitorHint.iteration_count() << endl;
	}else{
		if(!ANALYSIS) cout << "Hint did not converge" << endl;
	}

	if(monitorZero.converged()){
		if(!ANALYSIS) cout << "Zero converged under " << monitorZero.iteration_count() << endl;
	}else{
		if(!ANALYSIS) cout << "Zero did not converge" << endl;
	}

	if(ANALYSIS){
		cout << cuspZeroTime/(tridiagTime+cuspHintTime) << " " << rows << " " << nnzPerc << " " << bandPerc << " " << h_qband_mat.nnz << " " << h_band_mat.bandwidth << " ";
		cout << monitorZero.iteration_count() << " " << cuspZeroTime << " " << monitorHint.iteration_count() << " " << tridiagTime+cuspHintTime << " " << tridiagTime << " " << cuspHintTime << endl;
	}else{
		cout << "My time: " << tridiagTime+cuspHintTime << " (" << tridiagTime << "," << cuspHintTime << ")" << endl;
   		cout << "Bnc tym: " << cuspZeroTime << endl;	
	}


	cusparseDestroy(cusparse_handle);



}
