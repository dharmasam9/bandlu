/*
Ax=b where A is quasi-triangular matrix.
Comparing two iterative methods to solve x.
1: Start with zero vector as the base iteration.
2: Start with Tr(A)x=b as the base solution.

Compare number of iterations and running times.

// Things to meddle
* Generating values of A 
* Generating values of main diagonal of A
* Generating values of B

*/
#include <iostream>

#include <stdlib.h>
#include <time.h>

#include <cuda_runtime.h>
#include <cusparse_v2.h>

#include "timer.h"
#include "common.h"

using namespace std;

// CUSP related
#include <cusp/hyb_matrix.h>
#include <cusp/monitor.h>
#include <cusp/gallery/poisson.h>
#include <cusp/krylov/gmres.h>

// where to perform the computation
typedef cusp::device_memory MemorySpace;

// which floating point type to use
typedef double ValueType;

double get_random_value(){
	return rand()%10+3;
}


// Creates Ax = b(1):b(Bcount)
int create_qtridiag_solver(csr_matrix &sparse_mat, int rows, double nnzPercentage, double* &h_dl, double* &h_d, double* &h_du, double* &h_B, int bCount){
	int nnz;

	nnz = (nnzPercentage*rows*rows)/100.0;

	if(nnz < 3*rows-2){
		return -1;
	}

	cout << "$" << rows << " " << nnz << " " << nnz/(3*rows-2) << endl;

	// Creating tri diagonal matrix (except middle)
	h_dl = (double*) calloc(rows, sizeof(double));
	h_d = (double*) calloc(rows, sizeof(double));
	h_du = (double*) calloc(rows, sizeof(double));

	for (int i = 0; i < rows; ++i)
	{
		h_dl[i] = get_random_value();
		h_du[i] = get_random_value();
	}

	h_dl[0] = 0;
	h_du[rows-1] = 0;


	// Creating Sparse part
	vector<int> locations;

	int* rowPtr = (int*) calloc(rows+1, sizeof(int));
	int* colIndex = (int*) calloc(nnz, sizeof(int));
	double* values = (double*) calloc(nnz, sizeof(double));

	// Pushing locations of tri-diagonal in to locations

	// First row
	rowPtr[0] += 2;
	locations.push_back(0);
	locations.push_back(1);

	// Middle rows
	for (int i = 1; i < rows-1; ++i)
	{
		locations.push_back(i*rows + (i-1));
		locations.push_back(i*rows + i);
		locations.push_back(i*rows + (i+1));

		rowPtr[i] += 3;
	}

	// Last row
	rowPtr[rows-1] +=2;
	locations.push_back((rows-1)*rows + (rows-2));
	locations.push_back((rows-1)*rows + (rows-1));
	

	int filled = 0;
	int r,c;
	while(filled < nnz-(3*rows)+2){
		r = rand()%rows;
		c = rand()%rows;
		if(abs(c-r) > 1){
			if(find(locations.begin(), locations.end(), r*rows+c) == locations.end()){
				locations.push_back(r*rows + c);
				rowPtr[r]++;
				filled++;
			}
		}
	}

	// Ordering locations
	sort(locations.begin(),locations.end());

	// Generating rowPtr;
	int temp;
	int sum = 0;
	for (int i = 0; i <= rows; ++i)
	{
		temp = rowPtr[i];
		rowPtr[i] = sum;
		sum += temp;
	}
	
	double* row_sums = (double*) calloc(rows, sizeof(double));

	vector<int> middle;

	// Filling up column indices and values
	for (int i = 0; i < rows; ++i)
	{	
		r = i;
		for (int j = rowPtr[i]; j < rowPtr[i+1]; ++j)
		{
			c = locations[j]%rows;

			colIndex[j] = c;	
			//cout << "(" << r << "," << c << ")" << endl;
			int diaIndex = c-r;
			switch(diaIndex){
				case -1:
					values[j] = h_dl[r];
					row_sums[r] += values[j];
					break;
				case 0:
					middle.push_back(j);
					break;
				case 1:
					values[j] = h_du[r];
					row_sums[r] += values[j];
					break;
				default:
					values[j] = get_random_value();
					row_sums[r] += values[j];
			}
		}
	}

	for (int i = 0; i < rows; ++i)
	{
		h_d[i] = row_sums[i] + get_random_value();
		values[middle[i]] = h_d[i];
	}


	// Filling B matrix
	h_B = (double*) calloc(rows*bCount, sizeof(double));

	for (int i = 0; i < rows*bCount; ++i)
	{
		h_B[i] =  get_random_value() + 3;
	}


	
	// Populating csr matrix structure
	sparse_mat.rows = rows;
	sparse_mat.cols = rows;
	sparse_mat.nnz = nnz;
	sparse_mat.rowPtr = rowPtr;
	sparse_mat.colIndex = colIndex;
	sparse_mat.values = values;


	return 1;

}

void print_tridiagonal_matrix(int rows, double* h_dl, double* h_d, double* h_du, double* h_B, int bCount){
	double zero = 0;

	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < rows; ++j)
		{
			switch(j-i){
				case -1:
					cout << h_dl[i] << " ";
					break;
				case 0:	
					cout << h_d[i] << " ";
					break;
				case 1:
					cout << h_du[i] << " ";
					break;
				default:
					cout << zero << " ";
			}
		}
		cout << endl;
	}

	for (int i = 0; i < rows; ++i)
	{
		cout << h_B[i] << endl;
	}
}

void print_vector(double* a, int size){
	for (int i = 0; i < size; ++i)
	{
		cout << a[i] << endl;
	}
}

int main(int argc, char const *argv[])
{
	GpuTimer tridiagTimer;
	GpuTimer cuspZeroTimer;
	GpuTimer cuspHintTimer;

	double tridiagTime = 0;
	double cuspZeroTime = 0;
	double cuspHintTime = 0;

	int rows = 5;
	double nnzPercentage = 52;
	float nonBandFactor = 1;
	int bCount = 1;

	bool ANALYSIS = false;

	if(argc > 1) // number of rows
		rows = atoi(argv[1]);

	if(argc > 2)
		bCount = atoi(argv[2]);

	if(argc > 3){ // nnz Percentage
		nonBandFactor = atof(argv[3]);
		nnzPercentage = ((3*rows-2)*(1+nonBandFactor) * 100.0) /(rows*rows);
	}else{
		nnzPercentage = ((3*rows-2)*(1+nonBandFactor) * 100.0) /(rows*rows);
	}

	if(argc > 4 && argv[4][0] == '1' ) // Analysis
		ANALYSIS = true;


	srand (time(NULL));

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

    double* h_dl, *h_d, *h_du;
    double* d_dl, *d_d, *d_du;
    double* h_B, *d_B;
    
    csr_matrix h_mat, d_mat;

  	int status = create_qtridiag_solver(h_mat, rows, nnzPercentage, h_dl, h_d, h_du, h_B, bCount);
  	if(status == -1){
  		cout << "Cannot create a quasi tri diagonal matrix" << endl;
  		return 0;
  	}
  	create_device_csr_matrix(h_mat, d_mat);
    
    // print_tridiagonal_matrix(rows, h_dl, h_d, h_du, h_B, bCount);
    // print_host_csr_matrix(h_mat);

    // return 0;

    // Allocate memory on device and transfer from host
    cudaMalloc((void**)&d_dl, rows*sizeof(double));
    cudaMalloc((void**)&d_d, rows*sizeof(double));
    cudaMalloc((void**)&d_du, rows*sizeof(double));
    cudaMalloc((void**)&d_B, bCount * rows * sizeof(double));

    cudaMemcpy(d_dl, h_dl, rows*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_d, h_d, rows*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_du, h_du, rows*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, h_B, bCount * rows * sizeof(double), cudaMemcpyHostToDevice);

    //printf("COMPUTATION STARTS\n");

    tridiagTimer.Start();
    cusparseDgtsv(cusparse_handle,
    			  rows,
    			  bCount,
    			  d_dl, d_d, d_du,
    			  d_B, rows);
    cudaDeviceSynchronize();
   	tridiagTimer.Stop();

   
    double* h_X = (double*) calloc(bCount*rows, sizeof(double));
    cudaMemcpy(h_X, d_B, bCount*rows*sizeof(double), cudaMemcpyDeviceToHost);

    //print_vector(h_X, rows);
   
    // Iterative module for sparse matrix (with a starting point)
    // create an empty sparse matrix structure (HYB format)
    cusp::csr_matrix<int,double,cusp::host_memory> h_A(rows, rows, h_mat.nnz);

    // Transferring to cusp host csr structure
    for (int i = 0; i < h_mat.nnz; ++i)
    {
    	h_A.column_indices[i] = h_mat.colIndex[i];
    	h_A.values[i] = h_mat.values[i];
    }

    for (int i = 0; i < rows+1; ++i)
    {
    	h_A.row_offsets[i] = h_mat.rowPtr[i];
    }

    cusp::csr_matrix<int,double,cusp::device_memory> d_A = h_A;

    // print_host_csr_matrix(h_mat);
    // cusp::print(d_A);

    // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<double, cusp::host_memory> x(rows);
    cusp::array1d<double, cusp::host_memory> b(rows);

    // Transferring stuff to cups b array.
    for (int i = 0; i < rows; ++i)
    {
    	x[i] = h_X[i];
    	b[i] = h_B[i];
    }

   // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<double, cusp::device_memory> d_x = x;
    cusp::array1d<double, cusp::device_memory> d_b = b;
    cusp::array1d<double, cusp::device_memory> d_b_zero = b;


    
    // set stopping criteria:
    int  iteration_limit    = 500;
    double  relative_tolerance = 1e-6;

    cusp::monitor<double> monitorHint(d_b, iteration_limit, relative_tolerance,0, !ANALYSIS);
    int restart = 500;
    // solve the linear system A * x = b with the GMRES
    cuspHintTimer.Start();
    	cusp::krylov::gmres(d_A, d_x, d_b,restart, monitorHint);
    cuspHintTimer.Stop();
    cudaDeviceSynchronize();

    // x with zero as starting point
	cusp::array1d<double, cusp::device_memory> d_x_zero(rows, double(0));
	cusp::monitor<double> monitorZero(d_b_zero, iteration_limit, relative_tolerance,0, !ANALYSIS);

	cuspZeroTimer.Start();
		cusp::krylov::gmres(d_A, d_x_zero, d_b_zero,restart, monitorZero);
	cuspZeroTimer.Stop();
	cudaDeviceSynchronize();

	tridiagTime = tridiagTimer.Elapsed();
	cuspHintTime = cuspHintTimer.Elapsed();
	cuspZeroTime = cuspZeroTimer.Elapsed();


	if(monitorHint.converged()){
		if(!ANALYSIS) cout << "Hint converged under " << monitorHint.iteration_count() << endl;
	}else{
		if(!ANALYSIS) cout << "Hint did not converge" << endl;
	}

	if(monitorZero.converged()){
		if(!ANALYSIS) cout << "Zero converged under " << monitorZero.iteration_count() << endl;
	}else{
		if(!ANALYSIS) cout << "Zero did not converge" << endl;
	}

	float speedup = cuspZeroTime/(tridiagTime+cuspHintTime);
	float clever_time = tridiagTime+cuspHintTime;
	int clever_iterations = monitorHint.iteration_count();
	int bench_iterations = monitorZero.iteration_count();


	if(ANALYSIS){
		cout << "#" << rows << " " << nnzPercentage << " " << 100 << " "
         << speedup << " " << clever_time << " " << cuspZeroTime << " " << clever_iterations << " " << bench_iterations << " " << tridiagTime << " " << cuspHintTime << endl;
	}else{
		cout << "Speedup: " << cuspZeroTime/(tridiagTime+cuspHintTime) << endl;
		cout << "My time: " << tridiagTime+cuspHintTime << " (" << tridiagTime << "," << cuspHintTime << ")" << endl;
   		cout << "Bnc tym: " << cuspZeroTime << endl;	
	}
   	
    cusparseDestroy(cusparse_handle);

    return 0;

}