#include <cusp/io/matrix_market.h>
#include <cusp/csr_matrix.h>
#include <cusp/print.h>

#include <cuda_runtime.h>
#include <cusparse_v2.h>

#include <cusp/monitor.h>
#include <cusp/krylov/cg.h>
#include <cusp/krylov/bicgstab.h>
#include <cusp/krylov/gmres.h>
#include <cusp/gallery/poisson.h>
#include <cusp/gallery/random.h>

#include <iostream>
#include <stdlib.h>

#include "timer.h"
#include "common.h"

#include <fstream>


using namespace std;

void generate_cusp_from_csr(csr_matrix h_mat, cusp::csr_matrix<int, double, cusp::host_memory> &h_cusp_mat){
    h_cusp_mat.resize(h_mat.rows, h_mat.rows, h_mat.nnz);

    for (int i = 0; i < h_cusp_mat.num_entries; ++i)
    {
        h_cusp_mat.column_indices[i] = h_mat.colIndex[i];
        h_cusp_mat.values[i] = h_mat.values[i];
    }

    for (int i = 0; i <= h_cusp_mat.num_rows; ++i)
    {
        h_cusp_mat.row_offsets[i] = h_mat.rowPtr[i];
    }

}

inline bool does_file_exist (const std::string& name) {
    ifstream f(name.c_str());
    if (f.good()) {
        f.close();
        return true;
    } else {
        f.close();
        return false;
    }   
}

int main(int argc, char const *argv[])
{

    srand(time(NULL));

	int FROM_FILE = 0;
    bool ANALYSIS = true;

    int iteration_limit    = 500;
    float relative_tolerance = 1e-6;

	GpuTimer tridiagTimer;
	GpuTimer cuspZeroTimer;
	GpuTimer cuspHintTimer;

	// load A from disk into a csr_matrix
    cusp::csr_matrix<int, double, cusp::host_memory> h_A;

    // right hand side b
    double* h_b,*d_b;

    // cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

	if(argc > 1)
		FROM_FILE = atoi(argv[1]);

    double nnzPerc, bandPerc;
    int rows;
    int dia_count = 3;

	if(FROM_FILE){
		cusp::io::read_matrix_market_file(h_A, argv[2]);

        bool make_dia_dominant = true;

        if(argc > 3){
            if(atoi(argv[3]))
                make_dia_dominant = true;
            else
                make_dia_dominant = false;
        }
            
        if(make_dia_dominant){
            // Make the matrix diagonally dominant
            for (int i = 0; i < h_A.num_rows; ++i)
            {
                double sum = 0;
                int position = -1;
                for (int j = h_A.row_offsets[i]; j < h_A.row_offsets[i+1]; ++j)
                {
                    sum += abs(h_A.values[j]);
                    if(h_A.column_indices[j] == i)
                        position = j;
                }
                if(position != -1){
                    h_A.values[position] = sum;
                }


                // if(i < 20) cout << sum << endl;
            }    
        }

        // iteration count
        if(argc > 4)
            iteration_limit = atoi(argv[4]);

        // relative tolerance
        if(argc > 5)
            relative_tolerance = pow(10,-1*atoi(argv[5]));
        

        // Allocating memory for B
        h_b = (double*) calloc(h_A.num_cols, sizeof(double));
        cudaMalloc((void**)&d_b, h_A.num_rows*sizeof(double));


        // Get the file path
        string mat_file_path(argv[2]);
        string b_file_path = mat_file_path.substr(0, mat_file_path.size()-4);
        b_file_path += "_b.mtx";


        if(does_file_exist(b_file_path)){
            // load A from disk into a csr_matrix
            cusp::csr_matrix<int, double, cusp::host_memory> cusp_B;
            cusp::io::read_matrix_market_file(cusp_B, b_file_path);

            for (int i = 0; i < cusp_B.num_rows; ++i)
            {
                for (int j = cusp_B.row_offsets[i]; j < cusp_B.row_offsets[i+1]; ++j)
                {
                    h_b[i] = cusp_B.values[j];
                }
            }

        }else{
            // Randomly generate rhs
            for (int i = 0; i < h_A.num_rows; ++i)
                h_b[i] = rand()%10 + 2;
        }

        cudaMemcpy(d_b, h_b, sizeof(double)*h_A.num_rows, cudaMemcpyHostToDevice);
	}else{
        int bCount;
        int A_MAIN_DIAG_AMPLIFIER, B_AMPLIFIER;
        int VARIANCE_QUOTA;
        int RANGE;

        band_matrix h_band_mat;
        csr_matrix h_sparse_mat, h_qband_mat;

        bCount = 1;
        RANGE = 100;
        VARIANCE_QUOTA = 50;
        A_MAIN_DIAG_AMPLIFIER = 1;
        B_AMPLIFIER = 1;
        

        rows = atoi(argv[2]);
        //nnzPerc = atof(argv[3]);
        dia_count = atoi(argv[3]);
        bandPerc = atof(argv[4]);

        nnzPerc = (dia_count * 10000.0)/ (bandPerc*rows);

        if(argc > 5 && argv[5][0] == '1')
            ANALYSIS = true;
        
        if(argc > 6)
            RANGE = atoi(argv[6]);

        if(argc > 7)
            VARIANCE_QUOTA = atoi(argv[7]);

        if(argc > 8){
            A_MAIN_DIAG_AMPLIFIER = atoi(argv[8]);
        }

        if(argc > 9){
            B_AMPLIFIER = atoi(argv[9]);
        }


        h_b = (double*) calloc(rows, sizeof(double));
        cudaMalloc<double>(&d_b, rows*sizeof(double));

        if(!create_solver(h_band_mat, h_sparse_mat, h_qband_mat, rows, nnzPerc, bandPerc, h_b, bCount, RANGE, VARIANCE_QUOTA, A_MAIN_DIAG_AMPLIFIER, B_AMPLIFIER))
            return 0;

        cudaMemcpy(d_b, h_b, rows*sizeof(double), cudaMemcpyHostToDevice);

        // Convert host quasiband matrix to cusp quasi band matrix
        generate_cusp_from_csr(h_qband_mat, h_A);

	}

	// Transferring matrix on to device
    cusp::csr_matrix<int, double, cusp::device_memory> d_A(h_A);

    double* h_l, *h_m, *h_u;
    double* d_l, *d_m, *d_u;

    h_l = (double*) calloc(h_A.num_rows, sizeof(double));
    h_m = (double*) calloc(h_A.num_rows, sizeof(double));
    h_u = (double*) calloc(h_A.num_rows, sizeof(double));
    

    // Filling tri diagonal part
    for (int i = 0; i < h_A.num_rows; ++i)
    {
    	for (int j = h_A.row_offsets[i]; j < h_A.row_offsets[i+1]; ++j)
    	{
    		if(i == h_A.column_indices[j]+1){
    			h_l[i] = h_A.values[j];
    		}

    		if(i == h_A.column_indices[j]){
    			h_m[i] = h_A.values[j];
    		}

    		if(i == h_A.column_indices[j]-1){
    			h_u[i] = h_A.values[j];
    		}
    	}
    }


    // Transfering tri diagoanl arrays to device.
    cudaMalloc((void**)&d_l, h_A.num_rows*sizeof(double));
    cudaMalloc((void**)&d_m, h_A.num_rows*sizeof(double));
    cudaMalloc((void**)&d_u, h_A.num_rows*sizeof(double));

    cudaMemcpy(d_l, h_l, sizeof(double)*h_A.num_rows, cudaMemcpyHostToDevice);
    cudaMemcpy(d_m, h_m, sizeof(double)*h_A.num_rows, cudaMemcpyHostToDevice);
    cudaMemcpy(d_u, h_u, sizeof(double)*h_A.num_rows, cudaMemcpyHostToDevice);


    tridiagTimer.Start();
	    cusparseDgtsv(cusparse_handle,
	    			  h_A.num_rows,
	    			  1,
	    			  d_l, d_m, d_u,
	    			  d_b, h_A.num_rows);
	    cudaDeviceSynchronize();
    tridiagTimer.Stop();

    double* h_tr_sol = (double*) calloc(h_A.num_cols, sizeof(double));
    cudaMemcpy(h_tr_sol, d_b, sizeof(double)* h_A.num_rows, cudaMemcpyDeviceToHost);

    // Solving CG using tridiag

	// allocate storage for solution (x) and right hand side (b)
	cusp::array1d<double, cusp::host_memory> cusp_h_x(h_A.num_cols);
	cusp::array1d<double, cusp::host_memory> cusp_h_b(h_A.num_rows);

	for (int i = 0; i < h_A.num_rows; ++i)
	{
		cusp_h_x[i] = h_tr_sol[i];
		cusp_h_b[i] = h_b[i];
	}

    // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<double, cusp::device_memory> cusp_d_clever_x(cusp_h_x);
    cusp::array1d<double, cusp::device_memory> cusp_d_zero_x(h_A.num_cols, 0);

    cusp::array1d<double, cusp::device_memory> cusp_d_clever_b(cusp_h_b);
    cusp::array1d<double, cusp::device_memory> cusp_d_zero_b(cusp_h_b);

    // set stopping criteria:
    
    cusp::monitor<double> cleverMonitor(cusp_d_clever_b, iteration_limit, relative_tolerance, 0, !ANALYSIS);
    cusp::monitor<double> zeroMonitor(cusp_d_zero_b, iteration_limit, relative_tolerance, 0, !ANALYSIS);

    //cusp::print(d_A);
    //cusp::print(cusp_d_zero_b);

    // solve the linear system A * x = b with the Conjugate Gradient method
    cuspZeroTimer.Start();
        //cusp::krylov::bicgstab(d_A, cusp_d_zero_x, cusp_d_zero_b, zeroMonitor);
        cusp::krylov::gmres(d_A, cusp_d_zero_x, cusp_d_zero_b, iteration_limit, zeroMonitor);
        cudaDeviceSynchronize();
    cuspZeroTimer.Stop();
    

    cuspHintTimer.Start();
    	//cusp::krylov::bicgstab(d_A, cusp_d_clever_x, cusp_d_clever_b, cleverMonitor);
        cusp::krylov::gmres(d_A, cusp_d_clever_x, cusp_d_clever_b, iteration_limit, cleverMonitor);
        cudaDeviceSynchronize();
    cuspHintTimer.Stop();
    


    double tridiagTime = tridiagTimer.Elapsed();
    double cuspHintTime = cuspHintTimer.Elapsed();
    double cuspZeroTime = cuspZeroTimer.Elapsed();

    double clever_time = tridiagTime+cuspHintTime;
    double speedup = cuspZeroTime/clever_time;
    int clever_iterations = cleverMonitor.iteration_count();
    int bench_iterations = zeroMonitor.iteration_count();


    if(ANALYSIS){
        cout.precision(2);

        cout << fixed << "#" << rows << " " << dia_count << " " << bandPerc << " "
         << speedup << " " << (bench_iterations-clever_iterations) << " " << clever_time << " " << cuspZeroTime << " " << clever_iterations << " " << bench_iterations << " " << tridiagTime << " " << cuspHintTime << endl;
        cout << "*" << (bench_iterations-clever_iterations) << endl;

        /*
        cout  << speedup << " " << (bench_iterations-clever_iterations) << endl;
        cout << fixed << setw(2) << clever_iterations << " " << setw(5) <<  (cuspHintTime+tridiagTime) << " " << (cuspHintTime/clever_iterations) <<  endl;
        cout << fixed << setw(2) << bench_iterations << " " <<  setw(5) << cuspZeroTime << " " << (cuspZeroTime/bench_iterations) << endl;
        */

    }else{
        cout <<  speedup << endl;
        cout << "My time: " << tridiagTime+cuspHintTime << " (" << tridiagTime << "," << cuspHintTime << ")" << " in iterations " << cleverMonitor.iteration_count() <<  endl;
        cout << "Bnc tym: " << cuspZeroTime << " in iterations " << zeroMonitor.iteration_count() << endl;    
    }

	return 0;
}