#!/bin/bash

mats_dir="../testmatrices"
tars_dir="../tars"
mat_group="$1"
mat_name="$2"

mkdir "$tars_dir"
wget "-P" "$tars_dir" "http://www.cise.ufl.edu/research/sparse/MM/${mat_group}/${mat_name}.tar.gz" 
tar "-xf" "${tars_dir}/${mat_name}.tar.gz" 
mv "${mat_name}/${mat_name}.mtx" "$mats_dir"

if test -e "${mat_name}/${mat_name}_b.mtx"; then
	mv "${mat_name}/${mat_name}_b.mtx" "$mats_dir"
fi
rm "-rf" "${mat_name}"
rm "-rf" "$tars_dir"