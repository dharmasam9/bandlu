#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <vector>

#include <stdlib.h>
#include <assert.h>

#include <sys/time.h>

#include <cuda_runtime.h>

// CUSPARSE
#include <cusparse_v2.h>
#include <cublas_v2.h>
// #include <cusolverSp.h>

#include "timer.h"


using namespace std;

// device Prop variable
cudaDeviceProp deviceProp;


// Structure for storing diagonal band in matrix
typedef struct band_matrix{
	int rows;
	int cols;
	int nnz;  
	int bandwidth;
	double* data;
}band_matrix;

typedef struct csr_matrix
{
	int rows;
	int cols;
	int nnz;
	int* rowPtr;
	int* colIndex;
	double* values;
}csr_matrix;

typedef struct block_matrix
{
	int rows;
	int cols;
	double* data;
}block_matrix;

// For a given number of threads this function generates the kernel configuration 
// parameters with respect to the GPU used.
void find_dimensions_of_blocks_and_threads(int total_threads, dim3 &grids, dim3 &blocks){
	// If max threads per block is 1024 and max threads per sm is 2048 and alloted=max_threads_per_block (only 1 block is scheduled per sm)
	// If it is 512 atleast 2 will be scheduled

	int DEBUG = 0;

	int num_threads_per_block = deviceProp.maxThreadsPerBlock;
	int num_cores_per_sm = 192;

	if(deviceProp.major == 3){
		num_cores_per_sm = 192;
	}else{
		num_cores_per_sm = 32;
	}
	int num_sms = deviceProp.multiProcessorCount;

	if(total_threads < num_cores_per_sm){
		grids.x = 1; grids.y = 1; grids.z = 1;
		blocks.x = total_threads; blocks.y = 1; blocks.z = 1;
		if(DEBUG) printf("total_threads %d block dim (%d %d %d) thread dim (%d %d %d)\n",total_threads, grids.x,grids.y,grids.z, blocks.x,blocks.y,blocks.z  ); 
		return;
	}

	grids.x = num_sms;
	blocks.x = ceil(((double)total_threads)/grids.x);

	if(blocks.x > num_threads_per_block){
		blocks.x = num_threads_per_block;
		int block_size = blocks.x * blocks.y * blocks.z;

		grids.x = ceil(((double)total_threads)/block_size);

		if(grids.x > deviceProp.maxGridSize[0]){
			grids.y = ceil(((double)grids.x)/deviceProp.maxGridSize[0]);
			grids.x = deviceProp.maxGridSize[0];

			if(grids.y > deviceProp.maxGridSize[1]){
				grids.z = ceil(((double)grids.y)/deviceProp.maxGridSize[1]);
				grids.y = deviceProp.maxGridSize[1];

				if(grids.z > deviceProp.maxGridSize[2]){
					printf("Can't divide threads in to grids and blocks\n");
				}
			}
		}
	}

	if(DEBUG) printf("total_threads %d block dim (%d %d %d) thread dim (%d %d %d)\n",total_threads, grids.x,grids.y,grids.z, blocks.x,blocks.y,blocks.z  ); 

}

void print_host_block_matrix(double* h_values, int rows){
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < rows; ++j)
		{			
			printf("%9.3f ", h_values[i*rows+j]);
		}
		cout << endl;
	}
	cout << endl;
}

void print_device_block_matrix(double* d_values, int rows){
	double* h_values = (double*) calloc(rows*rows, sizeof(double));
	cudaMemcpy(h_values, d_values, rows*rows*sizeof(double), cudaMemcpyDeviceToHost);

	print_host_block_matrix(h_values,rows);

}

void print_host_double_array(double* h_values, int count){
	for (int i = 0; i < count; ++i)
		cout << h_values[i] << " ";
	cout << endl;
}

void print_device_double_array(double* d_values, int count){

	double* h_values = (double*) calloc(count, sizeof(double));

	cudaMemcpy(h_values, d_values, count*sizeof(double), cudaMemcpyDeviceToHost);

	for (int i = 0; i < count; ++i)
		cout << h_values[i] << " ";
	cout << endl;
}

void print_host_band_matrix(band_matrix mat){
	int dia_count = 2* mat.bandwidth -1;
	double zero = 0;
	for (int i = 0; i < mat.rows; ++i)
	{
		for (int j = 0; j < mat.cols; ++j)
		{
			int offset = j-i;

			if((offset > -1*mat.bandwidth) && (offset < mat.bandwidth)){
				printf("%9.3f", mat.data[(offset+mat.bandwidth-1)*mat.rows + i]);
			}else{
				printf("%9.3f", zero);
			}
		}
		cout << endl;
	}
	cout << endl;

}

void print_device_band_matrix(band_matrix mat){
	int dia_count = 2* mat.bandwidth -1;

	double* data = (double*) calloc(dia_count*mat.rows, sizeof(double));
	cudaMemcpy(data, mat.data, (dia_count*mat.rows)*sizeof(double), cudaMemcpyDeviceToHost);

	mat.data = data;
	print_host_band_matrix(mat);
}


void print_host_csr_matrix(csr_matrix mat){
	int index = 0;
	int count;
	double zero = 0;

	for (int i = 0; i < mat.rows; ++i)
	{
		count = mat.rowPtr[i+1] - mat.rowPtr[i];
		for (int j = 0; j < mat.rows; ++j)
		{
			if(mat.colIndex[index] == j && (count >0)){
				printf("%8.3f", mat.values[index]);
				index++;
				count--;
			}else{
				//printf("here\n");
				printf("%8.3f", zero);
			}
		}
		cout << endl;
	}
	cout << endl;
}


void print_device_csr_matrix(csr_matrix mat){
	int* rowPtr, *colIndex;
	double* values;


	rowPtr = (int*) calloc(mat.rows+1, sizeof(int));
	colIndex = (int*) calloc(mat.nnz, sizeof(int));
	values = (double*) calloc(mat.nnz, sizeof(double));

	cudaMemcpy(rowPtr, mat.rowPtr, (mat.rows+1) *sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(colIndex, mat.colIndex, (mat.nnz) *sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(values, mat.values, (mat.nnz) *sizeof(double), cudaMemcpyDeviceToHost);

	mat.rowPtr = rowPtr;
	mat.colIndex = colIndex;
	mat.values = values;

	print_host_csr_matrix(mat);
}

void merge_two_csr_parts(csr_matrix csr1, csr_matrix csr2, csr_matrix &csr){
	assert(csr1.rows == csr2.rows);
	assert(csr1.cols == csr2.cols);

	csr.rows = csr1.rows;
	csr.cols = csr1.cols;
	csr.nnz = csr1.nnz + csr2.nnz;

	csr.rowPtr = (int*) calloc(csr1.rows+1, sizeof(int));
	csr.colIndex = (int*) calloc(csr1.nnz + csr2.nnz, sizeof(int));
	csr.values = (double*) calloc(csr1.nnz + csr2.nnz, sizeof(double));


	// Adding two rowPtrs
	for (int i = 0; i <= csr1.rows; ++i)
	{
		csr.rowPtr[i] = csr1.rowPtr[i] + csr2.rowPtr[i];
	}

	int index =0;
	for (int i = 0; i < csr1.rows; ++i)
	{	
		int ptr1,ptr2;
		int last1,last2;

		ptr1 = csr1.rowPtr[i];
		ptr2 = csr2.rowPtr[i];

		last1 = csr1.rowPtr[i+1]-1;
		last2 = csr2.rowPtr[i+1]-1;

		while(ptr1 <= last1 && ptr2 <= last2){
			if(csr1.colIndex[ptr1] < csr2.colIndex[ptr2]){
				csr.colIndex[index] = csr1.colIndex[ptr1];
				csr.values[index] = csr1.values[ptr1];
				ptr1++;
			}else{
				csr.colIndex[index] = csr2.colIndex[ptr2];
				csr.values[index] = csr2.values[ptr2];
				ptr2++;
			}
			index++;
		}

		// Move left over in ptr1 to merge part
		if(ptr1 <= last1){
			for (int j = ptr1; j <= last1; ++j)
			{
				csr.colIndex[index] = csr1.colIndex[j];
				csr.values[index] = csr1.values[j];
				index++;
			}
		}

		// Move left over in ptr2 to merge part
		if(ptr2 <= last2){
			for (int j = ptr2; j <= last2; ++j)
			{
				csr.colIndex[index] = csr2.colIndex[j];
				csr.values[index] = csr2.values[j];
				index++;
			}
		}
	}

}

void create_csr_from_band(band_matrix band_mat, csr_matrix &csr_mat){
	// Populating and allocating
	csr_mat.rows = band_mat.rows;
	csr_mat.cols = band_mat.cols;
	csr_mat.nnz =  band_mat.nnz;

	csr_mat.rowPtr = (int*) calloc(band_mat.rows+1, sizeof(int));
	csr_mat.colIndex = (int*) calloc(band_mat.nnz, sizeof(int));
	csr_mat.values = (double*) calloc(band_mat.nnz, sizeof(double));


	int index = 0;
	int count,start_col,cur_col;
	double value;
	for (int i = 0; i < band_mat.rows; ++i)
	{
		count = min(band_mat.bandwidth-1, band_mat.cols-1-i) + min(band_mat.bandwidth-1,i) + 1;
		start_col = -1*min(band_mat.bandwidth-1,i) + i;

		for (int j = 0; j < count; ++j)
		{
			cur_col = start_col+j;
			value = band_mat.data[(cur_col-i+band_mat.bandwidth-1)*band_mat.rows + i];

			// Add only if the element is non-zero
			if(value != 0){
				csr_mat.rowPtr[i]++;
				csr_mat.colIndex[index] = cur_col;
				csr_mat.values[index] = value;
				index++;
			}
		}

	}

	// Generating rowPtr
	int sum = 0;
	int temp;
	for (int i = 0; i <= band_mat.rows; ++i)
	{	
		temp = csr_mat.rowPtr[i];
		csr_mat.rowPtr[i] = sum;
		sum += temp;
	}

}


// return value 0(cannot create) 1(success)
int create_solver(band_matrix &band_mat, csr_matrix &sparse_mat, csr_matrix &full_mat,
	int rows, 
	double nnzPerc, 
	double bandPerc, 
	double* b, 
	int bCount,
	int RANGE,
	int VARIANCE_QUOTA,
	int A_MAIN_DIAG_AMPLIFIER,
	int B_AMPLIFIER){
	// A is a quasi-band synthetic matrix with following properties:
	// * Onl one band around main diagonal
	// * bandOccupany is 100

	// Initializations
	srand(time(0) + A_MAIN_DIAG_AMPLIFIER+B_AMPLIFIER+(int)bandPerc + (int)nnzPerc);
	int BASE = ((100 - VARIANCE_QUOTA)*RANGE)/100;
	int VARIANCE = (VARIANCE_QUOTA*RANGE)/100 + 1;

	int A_VALUE_BASE = BASE;
	int B_VALUE_BASE = BASE;
	int A_VALUE_VARIANCE = VARIANCE;
	int B_VALUE_VARIANCE = VARIANCE;

	// BAND MATRIX

	int total_nnz, est_band_nnz, band_nnz, sparse_nnz;
	int bandwidth, dia_count;

	total_nnz = (nnzPerc*rows*rows)/100;
	est_band_nnz = (bandPerc*total_nnz)/100;

	bandwidth = ceil(est_band_nnz/(2.0*rows));
	dia_count = 2*bandwidth - 1;
	band_nnz = (2*bandwidth-1)*rows - (bandwidth*(bandwidth-1));
	sparse_nnz = total_nnz - band_nnz;

	// cout << bandwidth << " " << est_band_nnz << " " << total_nnz << " " << band_nnz << " " << sparse_nnz << endl;

	if(bandwidth < 2){
		cout << rows << " " << est_band_nnz << " " << bandwidth << endl;
		cout << "Increase nnzPerc or bandPerc" << endl;
		return 0;
	}

	// Allocate space
	band_mat.data = (double*) calloc(rows * dia_count, sizeof(double));

	// Populate band matrix structure
	band_mat.rows = rows;
	band_mat.cols = rows;
	band_mat.nnz = band_nnz;
	band_mat.bandwidth = bandwidth;

	int dia_num;
	double temp;
	double* row_sums = (double*) calloc(rows, sizeof(double));

	for (int i = -1*bandwidth + 1 ; i < bandwidth; ++i)
	{
		dia_num = i + bandwidth - 1;
		for (int j = abs(min(0,i)); j < min(rows, rows-i); ++j)
		{
			temp = A_VALUE_BASE + (rand()%A_VALUE_VARIANCE + 1);
			band_mat.data[dia_num*rows + j] = temp;
			row_sums[j] += temp;
		}
	}


	// SPARSE MATRIX
	vector<int> locations;
	int* rowPtr = (int*) calloc(rows+1, sizeof(int));
	int* colIndex = (int*) calloc(sparse_nnz, sizeof(int));
	double* values = (double*) calloc(sparse_nnz, sizeof(double));

	int filled = 0;
	int r,c;
	while(filled < sparse_nnz){
		r = rand()%rows;
		c = rand()%rows;
		if(abs(c-r) > bandwidth){
			if(find(locations.begin(), locations.end(), r*rows+c) == locations.end()){
				locations.push_back(r*rows + c);
				rowPtr[r]++;
				filled++;
			}
		}
	}

	// Ordering locations
	sort(locations.begin(),locations.end());

	// Filling up colIndex and values of sparse part
	for (int i = 0; i < sparse_nnz; ++i)
	{	
		temp = A_VALUE_BASE + (rand()%A_VALUE_VARIANCE + 1);
		values[i] = temp;
		colIndex[i] = locations[i]%rows;
		row_sums[locations[i]/rows] += temp;
	}

	// Modifying main diagonal to make it diagonally dominant.
	for (int i = 0; i < rows; ++i)
	{
		band_mat.data[(bandwidth-1)*rows+i] = A_MAIN_DIAG_AMPLIFIER * row_sums[i];
	}


	// Generating rowPtr;
	int temp_r;
	int sum = 0;
	for (int i = 0; i <= rows; ++i)
	{
		temp_r = rowPtr[i];
		rowPtr[i] = sum;
		sum += temp_r;
	}

	// Populating csr matrix structure
	sparse_mat.rows = rows;
	sparse_mat.cols = rows;
	sparse_mat.nnz = sparse_nnz;
	sparse_mat.rowPtr = rowPtr;
	sparse_mat.colIndex = colIndex;
	sparse_mat.values = values;


	// CONVERT BAND TO SPARSE AND MERGE
	csr_matrix band_as_csr_mat;
	create_csr_from_band(band_mat, band_as_csr_mat);

	if(bandPerc == 100)
		full_mat = band_as_csr_mat;
	else
		merge_two_csr_parts(band_as_csr_mat, sparse_mat, full_mat);

	cout << "$" << 2*bandwidth-1 << setw(10) << band_as_csr_mat.nnz << setw(10) << (full_mat.nnz-band_as_csr_mat.nnz) << setw(10) << full_mat.nnz << endl;

	// Create b1,b2, .. bCount in Ax=b...
	for (int i = 0; i < rows*bCount; ++i)
	{
		temp = B_VALUE_BASE + (rand()%B_VALUE_VARIANCE + 1);
		b[i] = B_AMPLIFIER * temp;
	}

	return 1;
}


void create_device_csr_matrix(csr_matrix host_mat, csr_matrix &device_mat){
	device_mat.rows = host_mat.rows;
	device_mat.cols = host_mat.cols;
	device_mat.nnz = host_mat.nnz;

	cudaMalloc((void**)&device_mat.rowPtr, sizeof(int)*(host_mat.rows+1));
	cudaMalloc((void**)&device_mat.colIndex, sizeof(int)*(host_mat.nnz));
	cudaMalloc((void**)&device_mat.values, sizeof(double)*(host_mat.nnz));

	cudaMemcpy(device_mat.rowPtr, host_mat.rowPtr, sizeof(int)*(host_mat.rows+1), cudaMemcpyHostToDevice);
	cudaMemcpy(device_mat.colIndex, host_mat.colIndex, sizeof(int)*(host_mat.nnz), cudaMemcpyHostToDevice);
	cudaMemcpy(device_mat.values, host_mat.values, sizeof(double)*(host_mat.nnz), cudaMemcpyHostToDevice);
}

void create_device_band_matrix(band_matrix host_mat, band_matrix &device_mat){
	device_mat.rows = host_mat.rows;
	device_mat.cols = host_mat.cols;
	device_mat.nnz = host_mat.nnz;
	device_mat.bandwidth = host_mat.bandwidth;

	int dia_count = 2*host_mat.bandwidth - 1;

	cudaMalloc((void**)&device_mat.data, sizeof(double)*(dia_count*host_mat.rows));
	cudaMemcpy(device_mat.data, host_mat.data, sizeof(double)*(dia_count*host_mat.rows), cudaMemcpyHostToDevice);
}

void create_device_block_matrix(block_matrix host_mat, block_matrix &device_mat){
	device_mat.rows = host_mat.rows;
	device_mat.cols = host_mat.cols;

	cudaMalloc((void**)&device_mat.data, sizeof(double)*(host_mat.cols * host_mat.rows));
	cudaMemcpy(device_mat.data, host_mat.data, sizeof(double)*(host_mat.cols * host_mat.rows), cudaMemcpyHostToDevice);


}


__global__
void compute_upper_triangle_row(int row, int rows, int bandwidth, double* data, double* res){
	
	int dia_offset = threadIdx.x;
	int dia_index = dia_offset + bandwidth - 1;
	int dia_count = 2* bandwidth -1;

	int top_row,top_col;
	int left_row,left_col;

	top_row = row-1;
	top_col = dia_offset + row;

	left_row = row;
	left_col = -1 + row;

	
	double sum = 0;
	while( (top_row >= 0) &&  (left_col >= 0) && (top_col-top_row) < bandwidth && (left_col - left_row) > -1*bandwidth){
		sum += res[(top_col-top_row + bandwidth - 1)*rows + top_row] * res[(left_col-left_row + bandwidth - 1)*rows + left_row];
		top_row--;
		left_col--;
	}

	res[dia_index*rows + row] = data[dia_index*rows + row] - sum;

}

__global__
void compute_lower_triangle_column(int col, int rows, int bandwidth, double* data, double* res){

	int dia_offset = -1*(threadIdx.x + 1);
	int dia_index = dia_offset + bandwidth - 1;
	int dia_count = 2*bandwidth - 1;

	int left_col, left_row;
	int top_col, top_row;

	left_col = col-1;
	left_row = col - dia_offset;

	top_col = col;
	top_row = col - 1;

	double sum = 0;

	while( (top_row >= 0) &&  (left_col >= 0) && (top_col-top_row) < bandwidth && (left_col - left_row) > -1*bandwidth){
		sum += res[(top_col-top_row + bandwidth - 1)*rows + top_row] * res[(left_col-left_row + bandwidth - 1)*rows + left_row];
		top_row--;
		left_col--;
	}

	res[dia_index*rows + (col-dia_offset)] = (data[dia_index*rows + (col-dia_offset)] - sum) / res[(bandwidth-1)*rows + col];


}


void naive_check_lu(double* lu, band_matrix mat){
	int DEBUG = 0;

	double* L = (double*) calloc(mat.rows*mat.cols , sizeof(double));
	double* U = (double*) calloc(mat.rows*mat.cols , sizeof(double));
	double* LU = (double*) calloc(mat.rows*mat.cols , sizeof(double));

	// Populate L matrix
	for (int i = -1*(mat.bandwidth-1); i < 0; ++i)
	{
		for (int j = abs(i); j < mat.rows; ++j)
		{
			// j->row i->dia_offset
			L[j*mat.cols + (i+j)] = lu[(i+mat.bandwidth-1)*mat.rows + j];
		}
	}

	for (int i = 0; i < mat.rows; ++i)
	{
		L[i*mat.cols + i] = 1;
	}

	// Populate U matrix
	for (int i = 0; i < mat.bandwidth; ++i)
	{
		for (int j = 0; j < mat.rows-abs(i); ++j)
		{
			// j->row i->dia_offset
			U[j*mat.cols + (i+j)] = lu[(i+mat.bandwidth-1)*mat.rows + j];
		}
	}

	/*
	if(DEBUG){
		for (int i = 0; i < mat.rows; ++i)
		{
			for (int j = 0; j < mat.cols; ++j)
			{
				printf("%8.3f ", L[i*mat.cols + j]);
			}
			printf("\n");
		}
		printf("\n");

		for (int i = 0; i < mat.rows; ++i)
		{
			for (int j = 0; j < mat.cols; ++j)
			{
				printf("%8.3f ", U[i*mat.cols + j]);
			}
			printf("\n");
		}
		printf("\n");
	}
	*/

	// Perform L*U matrix
	for (int i = 0; i < mat.rows; ++i)
	{
		for (int j = 0; j < mat.cols; ++j)
		{
			double sum = 0;
			for (int k = 0; k < mat.rows; ++k)
			{
				sum += L[i*mat.cols + k] * U[k*mat.cols + j];
			}
			LU[i*mat.cols + j] = sum;
		}
	}

	// Check with original matrix

	for (int i = 0; i < mat.rows; ++i)
	{
		for (int j = 0; j < mat.cols; ++j)
		{
			cout << setw(6) << left << LU[i*mat.cols + j];
		}
		cout << endl;
	}
	cout << endl;
}

void naive_check_x(band_matrix h_mat,double* h_x,double* h_b){

	double* calc_b = (double*) calloc(h_mat.rows, sizeof(double));

	// Create a band matrix
	int dia_num;
	int row,col, dia_offset;
	for (int i = -1*h_mat.bandwidth + 1 ; i < h_mat.bandwidth; ++i)
	{
		dia_num = i + h_mat.bandwidth - 1;
		for (int j = abs(min(0,i)); j < min(h_mat.rows, h_mat.rows-i); ++j)
		{	
			dia_offset = i;
			row = j;
			col = row + dia_offset;
			calc_b[row] += h_mat.data[dia_num*h_mat.rows + j] * h_x[col];
		}
	}

	double err_acc = 0.1;
	int same = 1;
	int j;
	for (j = 0; j < h_mat.rows; ++j)
	{
		// cout << calc_b[j] << " " << h_b[j] << endl;
		if(abs(calc_b[j] - h_b[j]) > err_acc){
			same = 0;
			break;
		}
	}

	if(same){
		cout << "Solved correctly :)" << endl;
	}else{
		cout << "Solved incorreclty :(" << endl;
	}

}

__global__
void find_y(double* b, double* y, int row){
	y[row] = b[row] - y[row];

}

__global__
void update_y(double* data, double* y, double* b, int col, int bandwidth, int rows){
	int row = blockIdx.x*blockDim.x + threadIdx.x + col +1;

	// For the bottom part of l matirx
	if(row < rows){
		int dia_offset = col-row;
		int dia_index = dia_offset + bandwidth - 1;

		y[row] += data[dia_index*rows + row] * y[col];

	}

	
}

__global__
void find_x(double* data, double* y, double* x, int row, int bandwidth, int rows){
	int dia_index = bandwidth-1;
	x[row] = (y[row] - x[row])/data[dia_index*rows + row];
}

__global__
void update_x(double* data,double* x, double* y, int col, int bandwidth,int rows){
	int row = col - (blockIdx.x*blockDim.x + threadIdx.x) - 1;

	// For the bottom part of l matirx
	if(row >= 0){
		int dia_offset = col-row;
		int dia_index = dia_offset + bandwidth - 1;

		x[row] += data[dia_index*rows + row] * x[col];
	}

}


__global__
void check_solution(double* d_b, double* d_iter_b, double tolerance, int* d_done){
	int tid = blockIdx.x*blockDim.x + threadIdx.x;

	if(abs(d_b[tid] - d_iter_b[tid]) >= tolerance){
		d_done[0] = 1;
	}
}

__global__
void calculate_new_x(double* d_iter0_x,double* d_b,double* d_iter_b,double* d_dia_vec){
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	d_iter0_x[tid] += (d_b[tid] - d_iter_b[tid])/d_dia_vec[tid];
}


void spIterativeSolver(cusparseHandle_t cusparse_handle, int rows, double tolerance, int max_iters, 
	csr_matrix d_A_mat, double* d_b, double* d_dia_vec,double* h_init_x, double* d_fin_x, int &iter_taken, double &time_taken){

	int DEBUG = 0;
	GpuTimer solver_timer;

	// Describing matrix trype for cusparse
	cusparseMatDescr_t cusparse_descrA = 0;
	cusparseCreateMatDescr(&cusparse_descrA);        
	cusparseSetMatType(cusparse_descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrA, CUSPARSE_INDEX_BASE_ZERO);

	double* h_b = (double*) calloc(rows, sizeof(double));
	cudaMemcpy(h_b, d_b, sizeof(double)*(rows), cudaMemcpyDeviceToHost);

	double *h_iter_x, *d_iter_x, *h_iter_b ,*d_iter_b;
	cudaMalloc((void**)&d_iter_x, sizeof(double) * (rows));
	cudaMemcpy(d_iter_x, h_init_x, sizeof(double)*(rows), cudaMemcpyHostToDevice);

	h_iter_b = (double*) calloc(rows, sizeof(double));
	cudaMalloc((void**)&d_iter_b, sizeof(double) * (rows));
	cudaMemset(d_iter_b,0, sizeof(double) * (rows));
	
	int iters = 0;
	int* d_done, *h_done;
	double alpha = 1, beta=0;

	// Done malloc
	h_done = (int*) calloc(1,sizeof(int));
	cudaMalloc((void**)&d_done, sizeof(int));
	cudaMemset(d_done,0, sizeof(int));

	dim3 grids,blocks;
	find_dimensions_of_blocks_and_threads(rows, grids, blocks);

	solver_timer.Start();
		while(!h_done[0] && iters < max_iters){
			iters++;
			// Calling SPMV kernel
			cudaMemset(d_iter_b,0, sizeof(double)*rows);
			cusparseDcsrmv(cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
					d_A_mat.rows, d_A_mat.cols, d_A_mat.nnz, &alpha, cusparse_descrA,
					d_A_mat.values, d_A_mat.rowPtr, d_A_mat.colIndex,
					d_iter_x, &beta,
					d_iter_b);

			cudaDeviceSynchronize();

			
			// Checking whether solution is with in tolerance levels
			check_solution<<<grids,blocks>>>(d_b, d_iter_b, tolerance, d_done);
			cudaMemcpy(h_done, d_done, sizeof(int), cudaMemcpyDeviceToHost);
			cudaMemset(d_done,0, sizeof(int));

			h_done[0] = (h_done[0]+1)%2;

			// Calculating new x
			calculate_new_x<<<grids,blocks>>>(d_iter_x, d_b, d_iter_b, d_dia_vec);
			cudaDeviceSynchronize();

			double error = 0;
			cudaMemcpy(h_iter_b, d_iter_b, sizeof(double)*rows, cudaMemcpyDeviceToHost);

			for (int i = 0; i < rows; ++i)
			{
				error += abs(h_b[i]-h_iter_b[i]);
			}

			if(DEBUG) cout << "Iter" << iters << " " << error << endl;

		}
	solver_timer.Stop();

	iter_taken = iters;
	time_taken = solver_timer.Elapsed();

	cudaMemcpy(d_fin_x, d_iter_x, sizeof(double)*rows, cudaMemcpyDeviceToDevice);
	//print_host_double_array(h_iter_x, rows);

}

__global__
void compute_upper_dets(int rows, int bandwidth, int dia_count, int dia_index, double* data, double* d_dia_vec, double* dets){

	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = dia_index + i;


	if(i == j){
		dets[i*rows + j] = 1;		
		return;
	}

	double sum = 0;
	double mul = 1;
	int sign = 1;

	for (int t = 1; t <= (j-i) && (t < bandwidth); ++t)
	{

		sum += sign * data[ (bandwidth+t-1)*rows + (j-t)] * dets[i*rows + (j-t)]  * mul;
		mul *= data[(bandwidth-1)*rows + (j-t)];
		sign *= -1;

		if(dia_index == 3 && i < 0){
			printf("%f\n", sum);
		}
	}

	dets[i*rows + j] = sum;

}

__global__
void compute_lower_dets(int rows, int bandwidth, int dia_count, int dia_index, double* data, double* d_dia_vec, double* dets){
	int j = blockIdx.x*blockDim.x + threadIdx.x;
	int i = j - dia_index;

	if(i == j){
		dets[i*rows + j] = 1;
		return;
	}


	double sum = 0;
	int sign = 1;

	for (int t = 1; t <= (i-j) && (t < bandwidth); ++t)
	{
		sum += sign * data[ (bandwidth-t-1)*rows + (j+t)] * dets[i*rows + (j+t)];
		sign *= -1;

		if(dia_index == -1 && j <0){
			printf("%f\n", sum);
		}
	}

	dets[i*rows + j] = sum;
}

__global__
void find_inverse_of_L_and_U(int rows, double* d_dets, double* d_prefix_dia_vec){
	int tid = blockIdx.x*blockDim.x + threadIdx.x;

	int row,col;
	row = tid/rows;
	col = tid%rows;

	int sign = -2*((row+col)%2) + 1;

	double val = d_dets[row*rows + col];

	if(row > col){
		d_dets[row*rows + col] = val*sign;
	}else{
		d_dets[row*rows + col] = (val*sign)/(d_prefix_dia_vec[col+1]/d_prefix_dia_vec[row]);
	}
	
}

void convert_host_band_to_host_block(band_matrix band_mat, block_matrix &block_mat){
	block_mat.rows = band_mat.rows;
	block_mat.cols = band_mat.cols;

	block_mat.data = (double*) calloc(band_mat.rows * band_mat.rows, sizeof(double));

	int dia_count = 2*band_mat.bandwidth-1;
	int offset,row,col;
	for (int i = 1; i <= dia_count; ++i)
	{
		offset = i - band_mat.bandwidth;
		for (int j = 0; j < band_mat.rows; ++j)
		{	
			row = j;
			col = j+offset;
			if(col >= 0 && col < band_mat.cols){
				block_mat.data[row*band_mat.rows + col] =  band_mat.data[(offset+band_mat.bandwidth-1)*band_mat.rows + row];
			}
		}
	}
}

void convert_host_csr_to_host_block(csr_matrix csr_mat, block_matrix &block_mat){
	block_mat.rows = csr_mat.rows;
	block_mat.cols = csr_mat.cols;

	block_mat.data = (double*) calloc(csr_mat.rows * csr_mat.rows, sizeof(double));

	int count;

	for (int i = 0; i < csr_mat.rows; ++i)
	{
		for (int j = csr_mat.rowPtr[i]; j < csr_mat.rowPtr[i+1]; ++j)
		{
			block_mat.data[i*csr_mat.rows + csr_mat.colIndex[j]] = csr_mat.values[j];
		}
	}

}

void create_csr_mats_from_block_inverse(int rows, double* h_dets, csr_matrix &h_U_inv_mat, csr_matrix &h_L_inv_mat, 
			csr_matrix &d_U_inv_mat, csr_matrix &d_L_inv_mat){

	int max_nnz = (rows * (rows+1))/2;

	h_U_inv_mat.rows = rows; h_U_inv_mat.cols = rows;
	h_U_inv_mat.rowPtr = (int*) calloc(rows+1, sizeof(int));
	h_U_inv_mat.colIndex = (int*) calloc(max_nnz, sizeof(int));
	h_U_inv_mat.values = (double*) calloc(max_nnz, sizeof(double));

	h_L_inv_mat.rows = rows; h_L_inv_mat.cols = rows;
	h_L_inv_mat.rowPtr = (int*) calloc(rows+1, sizeof(int));
	h_L_inv_mat.colIndex = (int*) calloc(max_nnz, sizeof(int));
	h_L_inv_mat.values = (double*) calloc(max_nnz, sizeof(double));

	int U_parsed_nnz = 0, L_parsed_nnz = 0;

	// Creating csr for U inv matrix
	for (int i = 0; i < rows; ++i)
	{
		for (int j = i; j < rows; ++j)
		{
			if(h_dets[i*rows+j] != 0){
				h_U_inv_mat.colIndex[U_parsed_nnz] = j;
				h_U_inv_mat.values[U_parsed_nnz] = h_dets[i*rows+j];
				h_U_inv_mat.rowPtr[i]++;
				U_parsed_nnz++;
			}
		}
	}

	h_U_inv_mat.nnz = U_parsed_nnz;

	int temp;
	int sum = 0;
	for (int i = 0; i < rows+1; ++i)
	{
		temp = h_U_inv_mat.rowPtr[i];
		h_U_inv_mat.rowPtr[i] = sum;
		sum += temp;
	}

	// Creating csr for L inv matrix
	for (int i = 0; i < rows; ++i)
	{	

		for (int j = 0; j < i; ++j)
		{
			if(h_dets[i*rows+j] != 0){
				h_L_inv_mat.colIndex[L_parsed_nnz] = j;
				h_L_inv_mat.values[L_parsed_nnz] = h_dets[i*rows+j];
				h_L_inv_mat.rowPtr[i]++;
				L_parsed_nnz++;
			}
		}
		// Main Dia element is 1
		h_L_inv_mat.colIndex[L_parsed_nnz] = i;
		h_L_inv_mat.values[L_parsed_nnz] = 1;
		h_L_inv_mat.rowPtr[i]++;
		L_parsed_nnz++;

	}

	h_L_inv_mat.nnz = L_parsed_nnz;

	sum = 0;
	for (int i = 0; i < rows+1; ++i)
	{
		temp = h_L_inv_mat.rowPtr[i];
		h_L_inv_mat.rowPtr[i] = sum;
		sum += temp;
	}

	// Creating csr matrices in device
	create_device_csr_matrix(h_L_inv_mat, d_L_inv_mat);
	create_device_csr_matrix(h_U_inv_mat, d_U_inv_mat);

}

void find_inverse_using_cublas(cublasHandle_t cublas_handle, int rows, double* d_A, double* d_invA, double &time_taken){
    GpuTimer timer;

    int batchSize = 1;

    double** h_Array = (double**) calloc(1, sizeof(double*));
    double** h_Crray = (double**) calloc(1, sizeof(double*));
    h_Array[0] = d_A;
    h_Crray[0] = d_invA;

    double** d_Array;
    double** d_Crray;

    cudaMalloc<double*>(&d_Array, sizeof(double*));
    cudaMalloc<double*>(&d_Crray, sizeof(double*));

    cudaMemcpy(d_Array, h_Array, sizeof(double*), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Crray, h_Crray, sizeof(double*), cudaMemcpyHostToDevice);


    // Allocate d_Carray

    int *P, *INFO;

    cudaMalloc<int>(&P,rows * batchSize * sizeof(int));
    cudaMalloc<int>(&INFO,batchSize * sizeof(int));

	timer.Start();

	    cublasDgetrfBatched(cublas_handle, 
	                        rows,
	                        d_Array,
	                        rows,
	                        P,
	                        INFO,
	                        batchSize);

	    const double** d_Array_const = (const double**)d_Array;
	    const int* P_const = P;

	    cublasDgetriBatched(cublas_handle,
	                        rows,
	                        d_Array_const,
	                        rows,
	                        P_const,
	                        d_Crray,
	                        rows,
	                        INFO,
	                        batchSize);

	    cudaDeviceSynchronize();
	timer.Stop();

	time_taken = timer.Elapsed();

}