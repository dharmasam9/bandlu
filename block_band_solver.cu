
// Create band matrix.
// Store rows/bandwidth blocks separately
// Solve the blocks separately and get an initial x.
// Use that initial x for GMRES.

#include <cuda_runtime.h>

#include <iostream>
#include "common.h"

// CUSP related
#include <cusp/hyb_matrix.h>
#include <cusp/monitor.h>
#include <cusp/krylov/gmres.h>


using namespace std;

void create_band_matrix(band_matrix &band_mat, float* h_blocks_ptr, float* h_b, int rows, int bandwidth, int block_bandwidth){
	int band_nnz = (2*bandwidth-1)*rows - (bandwidth*(bandwidth-1));
	int dia_count = 2*bandwidth -1;
	int block_size = block_bandwidth*block_bandwidth;

	// Allocate space
	band_mat.data = (float*) calloc(rows * dia_count, sizeof(float));

	// Populate band matrix structure
	band_mat.rows = rows;
	band_mat.cols = rows;
	band_mat.nnz = band_nnz;
	band_mat.bandwidth = bandwidth;

	int dia_num;
	float* neigh_sums = (float*) calloc(rows, sizeof(float));
	float temp;
	int row,col;
	int row_block_num, col_block_num;

	for (int i = -1*bandwidth + 1 ; i < bandwidth; ++i)
	{
		dia_num = i + bandwidth - 1;
		for (int j = abs(min(0,i)); j < min(rows, rows-i); ++j)
		{	
			row = j;
			col = j+i;

			temp = rand()%10 + 3;

			band_mat.data[dia_num*rows + j] = temp;
			neigh_sums[j] += temp;

			row_block_num = row/block_bandwidth;
			col_block_num = col/block_bandwidth; 

			if(row_block_num == col_block_num){
				h_blocks_ptr[((row_block_num)*block_size) + (row%block_bandwidth)*block_bandwidth + (col%block_bandwidth)] = temp;	
			}
			
		}
	}

	// Updating diagonal to make in diagonally dominant
	for (int i = 0; i < rows; ++i)
	{
		band_mat.data[(bandwidth-1)*rows + i] = neigh_sums[i];
		h_blocks_ptr[(i/block_bandwidth)*block_size + (i%block_bandwidth)*block_bandwidth + (i%block_bandwidth)] = neigh_sums[i];
	}

	for (int i = 0; i < rows; ++i)
	{
		h_b[i] = rand()%30 + 3;
	}

}

void print_host_blocks(float* h_blocks_data, int rows, int bandwidth){
	int num_blocks = rows/bandwidth;
	int block_size = bandwidth*bandwidth;

	for (int block_num = 0; block_num < num_blocks; ++block_num)
	{
		for (int row = 0; row < bandwidth; ++row)
		{
			for (int col = 0; col < bandwidth; ++col)
			{
				cout << h_blocks_data[(block_size*block_num) + (row*bandwidth) + col] << " ";
			}
			cout << endl;
		}
		cout << endl << endl;
	}
}

__global__
void find_initial_x_with_bbw_2(float* d_blocks_data, float* d_b, float* d_init_x, int num_blocks){
	int block_num = blockIdx.x*blockDim.x + threadIdx.x;
	float a00 = d_blocks_data[block_num*4];
	float a01 = d_blocks_data[block_num*4+1];
	float a10 = d_blocks_data[block_num*4+2];
	float a11 = d_blocks_data[block_num*4+3];

	float b0 = d_b[block_num*2];
	float b1 = d_b[block_num*2+1];

	if(block_num < num_blocks){
		d_init_x[block_num*2] = (b0*a11 - b1*a01)/(a00*a11 - a10*a01);
		d_init_x[block_num*2+1] = (b0*a10 - b1*a00)/(a01*a10 - a11*a00);
	}
}

int main(int argc, char const *argv[])
{
	// Initializing deviceProp variable
	cudaGetDeviceProperties(&deviceProp,0);
	dim3 grids,blocks;

	// Input
	// rows, bandwidth
	int rows,bandwidth;
	int num_blocks;
	int block_bandwidth,block_size;

	GpuTimer blockSolTimer, cuspHintTimer, cuspZeroTimer, tridiagTimer;
	float tridiagTime = 0;
	float blockSolTime = 0;
	float cuspHintTime = 0;
	float cuspZeroTime = 0;

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

    srand (time(NULL));

	rows = atoi(argv[1]);
	bandwidth = atoi(argv[2]);
	block_bandwidth = 2;
	rows =  (rows/block_bandwidth)*block_bandwidth; // Adjusting rows to bandwidth

	num_blocks = rows/block_bandwidth;
	block_size = block_bandwidth*block_bandwidth;

	// Initializing and allocting blocks
	band_matrix h_mat_dia;
	csr_matrix h_mat_csr;

	float* h_b, *d_b;
	float* h_blocks_data, *d_blocks_data;

	h_b = (float*) calloc(rows, sizeof(float));
	cudaMalloc<float>(&d_b, rows*sizeof(float));
	
	h_blocks_data = (float*) calloc(num_blocks*block_size, sizeof(float));
	cudaMalloc<float>(&d_blocks_data, num_blocks*block_size*sizeof(float));

	create_band_matrix(h_mat_dia, h_blocks_data, h_b, rows, bandwidth,block_bandwidth);
	create_csr_from_band(h_mat_dia, h_mat_csr);

	cudaMemcpy(d_b, h_b, rows*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_blocks_data, h_blocks_data, num_blocks*block_size*sizeof(float), cudaMemcpyHostToDevice);

	
	//print_host_band_matrix(h_mat_dia);
	//print_host_float_array(h_b,rows);
	//print_host_blocks(h_blocks_data, rows, block_bandwidth);

	float* h_init_x, *d_init_x;
	h_init_x = (float*) calloc(rows, sizeof(float));
	cudaMalloc<float>(&d_init_x, rows*sizeof(float));

	
	find_dimensions_of_blocks_and_threads(num_blocks, grids, blocks);
	blockSolTimer.Start();
		find_initial_x_with_bbw_2<<<grids,blocks>>>(d_blocks_data, d_b, d_init_x, num_blocks);
	blockSolTimer.Stop();

	cudaMemcpy(h_init_x, d_init_x, rows*sizeof(float), cudaMemcpyDeviceToHost);

	//print_host_float_array(h_init_x,rows);

	// TODO solve for tridiagonal
	float* d_dl,*d_d,*d_du;
	cudaMalloc((void**)&d_dl, rows*sizeof(float));
    cudaMalloc((void**)&d_d, rows*sizeof(float));
    cudaMalloc((void**)&d_du, rows*sizeof(float));

    cudaMemcpy(d_dl, &h_mat_dia.data[(h_mat_dia.bandwidth-2)*rows], rows*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_d, &h_mat_dia.data[(h_mat_dia.bandwidth-1)*rows], rows*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_du, &h_mat_dia.data[(h_mat_dia.bandwidth)*rows], rows*sizeof(float), cudaMemcpyHostToDevice);

    // Getting initial x from the tri diagonal solver
    tridiagTimer.Start();
    cusparseSgtsv(cusparse_handle,
    			  rows,
    			  1,
    			  d_dl, d_d, d_du,
    			  d_b, rows);
    cudaDeviceSynchronize();
   	tridiagTimer.Stop();

   	cudaMemcpy(h_init_x, d_b, rows*sizeof(float), cudaMemcpyDeviceToHost);

	// Solve it using CUSP
    cusp::csr_matrix<int,float,cusp::host_memory> h_A(rows, rows, h_mat_csr.nnz);

    // Transferring to cusp host csr structure
    for (int i = 0; i < h_mat_csr.nnz; ++i)
    {
    	h_A.column_indices[i] = h_mat_csr.colIndex[i];
    	h_A.values[i] = h_mat_csr.values[i];
    }

    for (int i = 0; i < rows+1; ++i)
    {
    	h_A.row_offsets[i] = h_mat_csr.rowPtr[i];
    }

    cusp::csr_matrix<int,float,cusp::device_memory> d_A = h_A;

    // print_host_csr_matrix(h_mat_csr);
    // cusp::print(d_A);

    // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<float, cusp::host_memory> x(rows);
    cusp::array1d<float, cusp::host_memory> b(rows);

    for (int i = 0; i < rows; ++i)
    {
    	x[i] = h_init_x[i];
    	b[i] = h_b[i];
    }

   // allocate storage for solution (x) and right hand side (b)
    cusp::array1d<float, cusp::device_memory> d_x = x;
    cusp::array1d<float, cusp::device_memory> d_b_hint = b;


    // set stopping criteria:
    int  iteration_limit    = 500;
    float  relative_tolerance = 1e-6;

    cusp::monitor<float> monitorHint(d_b_hint, iteration_limit, relative_tolerance);
    int restart = 500;

    // solve the linear system A * x = b with the GMRES
    cuspHintTimer.Start();
    	cusp::krylov::gmres(d_A, d_x, d_b_hint,restart, monitorHint);
    	cudaDeviceSynchronize();
    cuspHintTimer.Stop();

    // x with zero as starting point
	cusp::array1d<float, cusp::device_memory> d_x_zero(rows, float(0));
	cusp::monitor<float> monitorZero(d_b_hint, iteration_limit, relative_tolerance);

	cuspZeroTimer.Start();
		cusp::krylov::gmres(d_A, d_x_zero, d_b_hint,restart, monitorZero);
		cudaDeviceSynchronize();
	cuspZeroTimer.Stop();

	blockSolTime = blockSolTimer.Elapsed();
	tridiagTime = tridiagTimer.Elapsed();
	cuspHintTime = cuspHintTimer.Elapsed();
	cuspZeroTime = cuspZeroTimer.Elapsed();

	int ANALYSIS = 0;

	if(monitorHint.converged()){
		if(!ANALYSIS) cout << "Hint converged under " << monitorHint.iteration_count() << endl;
	}else{
		if(!ANALYSIS) cout << "Hint did not converge" << endl;
	}

	if(monitorZero.converged()){
		if(!ANALYSIS) cout << "Zero converged under " << monitorZero.iteration_count() << endl;
	}else{
		if(!ANALYSIS) cout << "Zero did not converge" << endl;
	}

	if(ANALYSIS){
		cout << cuspZeroTime/(tridiagTime+cuspHintTime) << " " << rows << " " << (h_mat_csr.nnz*100)/(rows*rows) << " " << h_mat_csr.nnz << " ";
		cout << monitorZero.iteration_count() << " " << cuspZeroTime << " " << monitorHint.iteration_count() << " " << tridiagTime+cuspHintTime << " " << tridiagTime << " " << cuspHintTime << endl;
	}else{
		cout << "My time: " << tridiagTime+cuspHintTime << " (" << tridiagTime << "," << cuspHintTime << ")" << endl;
   		cout << "Bnc tym: " << cuspZeroTime << endl;	
	}


	cusparseDestroy(cusparse_handle);


	return 0;
}
