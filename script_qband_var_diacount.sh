
# Synthetic matrix properites
rows=8000
dia_count=5
band_perc=100

num_graphs=4
row_mul=2
num_data_points=20
strip_size=2

result_dir="./results/"
hyphen="-"
mtype="qband"
exp="diaCountVar"
result=$result_dir$mtype$hyphen$exp$hyphen$rows$hyphen$band_perc


if [ -f "$result" ]
then
	rm $result
else
	touch $result
fi

for ((  k = 0 ;  k < num_graphs;  k++  ))
do
	dia_count=5
	for ((  j = 0 ;  j < num_data_points;  j++  ))
	do
		./qband "0" "$rows" "$dia_count" "$band_perc" "1" >> $result
		#echo "0" "$rows" "$dia_count" "$band_perc" "1"
		dia_count=$(echo "scale=2; ($dia_count+$strip_size)" | bc)
	done
	echo "" >> $result
	rows=$(echo "scale=2; ($rows*$row_mul)" | bc)
done