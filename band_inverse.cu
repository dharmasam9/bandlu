#include "common.h"

int main(int argc, char const *argv[])
{	
	// Initializing deviceProp variable
	cudaGetDeviceProperties(&deviceProp,0);
	dim3 grids,blocks;

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

    // cublas handle
    cublasHandle_t cublas_handle;
    cublasCreate(&cublas_handle);

	srand(time(0));
	int FROM_FILE = 0;

	GpuTimer lu_decomp_timer, upper_triag_timer, lower_triag_timer;
	GpuTimer sparse_correction_timer;
	GpuTimer benchmark_iter_timer, benchmark_timer;
	GpuTimer upper_dets_timer, lower_dets_timer, inverse_timer, base_x_timer;
	


	float lu_decomp_time = 0,upper_triag_time = 0, lower_triag_time = 0;
	float upper_dets_time = 0 , lower_dets_time = 0, inverse_time = 0, base_x_time = 0;
	float sparse_correction_time = 0;
	float benchmark_iter_time = 0;
	float cublas_band_inv_time = 0;


	int rows,bandwidth,dia_count;
	band_matrix h_band_mat, d_band_mat;
	csr_matrix h_sparse_mat, d_sparse_mat;
	csr_matrix h_qband_mat, d_qband_mat;

	float* h_b, *d_b;

	if(argc > 1){
		FROM_FILE = atoi(argv[1]);
	}

	if(FROM_FILE){
		ifstream myfile;
		myfile.open("input");
		myfile >> rows >> bandwidth;
		dia_count = 2*bandwidth - 1;

		// SETTING UP BAND MATRIX
		h_band_mat.rows = rows;
		h_band_mat.cols = rows;
		h_band_mat.bandwidth = bandwidth;

		h_band_mat.data = (float*) calloc(rows*dia_count, sizeof(float));

		// READING BAND MATRIX
		int offset;
		float elem;
		int band_nnz_count = 0;
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < rows; ++j)
			{
				offset = j-i;
				myfile >> elem;
				if(offset > -1*(bandwidth) && offset < bandwidth){
					h_band_mat.data[(offset+bandwidth-1)*h_band_mat.rows + i] = elem;
					if(elem != 0)  band_nnz_count++;
				}
			}
		}
		h_band_mat.nnz = band_nnz_count;

		// SETTING UP SPARSE MATRIX
		int sparse_nnz_count;
		myfile >> sparse_nnz_count;

		h_sparse_mat.rows = rows;
		h_sparse_mat.cols = rows;
		h_sparse_mat.nnz = sparse_nnz_count;

		h_sparse_mat.rowPtr = (int*) calloc(rows+1, sizeof(int));
		h_sparse_mat.colIndex = (int*) calloc(sparse_nnz_count, sizeof(int));
		h_sparse_mat.values = (float*) calloc(sparse_nnz_count, sizeof(float));

		int index = 0;
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < rows; ++j)
			{
				myfile >> elem;
				if(elem != 0){
					h_sparse_mat.colIndex[index] = j;
					h_sparse_mat.values[index] = elem;
					h_sparse_mat.rowPtr[i]++;
					index++;
				}
			}
		}

		// exclusive prefix sum on rowcount
		int temp;
		int sum = 0;
		for (int i = 0; i < rows+1; ++i)
		{
			temp = h_sparse_mat.rowPtr[i];
			h_sparse_mat.rowPtr[i] = sum;
			sum += temp;
		}

		// SETTING UP B VECTOR
		h_b = (float*) calloc(rows, sizeof(float));
		for (int i = 0; i < rows; ++i)
		{
			myfile >> h_b[i];
		}

		csr_matrix band_as_csr_mat;
		create_csr_from_band(h_band_mat, band_as_csr_mat);
		merge_two_csr_parts(band_as_csr_mat, h_sparse_mat, h_qband_mat);

	}else{
		rows = 7;
		float nnzPerc = 70;
		float bandPerc = 90;

		if(argc > 2){
			rows = atoi(argv[2]);
		}
		if(argc > 3){
			nnzPerc = atof(argv[3]);
		}
		if(argc > 4){
			bandPerc = atof(argv[4]);
		}


		h_b = (float*) calloc(rows, sizeof(float));
		create_solver(h_band_mat, h_sparse_mat, h_qband_mat, rows, nnzPerc, bandPerc, h_b);
		bandwidth = h_band_mat.bandwidth;
		dia_count = 2*h_band_mat.bandwidth - 1;

	}

	// print_host_band_matrix(h_band_mat);
	// print_host_csr_matrix(h_sparse_mat);
	// print_host_csr_matrix(h_qband_mat);

	// TRANSFERRING DATA TO DEVICE
	create_device_band_matrix(h_band_mat, d_band_mat);
	create_device_csr_matrix(h_sparse_mat, d_sparse_mat);
	create_device_csr_matrix(h_qband_mat, d_qband_mat);

	cudaMalloc((void**)&d_b, sizeof(float)*d_band_mat.rows);
	cudaMemcpy(d_b, h_b, sizeof(float)*d_band_mat.rows, cudaMemcpyHostToDevice);

	// Get the diaogonal vector.
	float* h_dia_vec,*d_dia_vec;
	h_dia_vec = (float*) calloc(h_band_mat.rows, sizeof(float));
	memcpy(h_dia_vec, &h_band_mat.data[(h_band_mat.bandwidth-1)*h_band_mat.rows], h_band_mat.rows*sizeof(float));

	cudaMalloc((void**)&d_dia_vec, sizeof(float) * (rows));
	cudaMemcpy(d_dia_vec, h_dia_vec, sizeof(float)*(rows), cudaMemcpyHostToDevice);

	// Prefix multiplicaiton of dia_vec of U matrix
	float* h_prefix_U_dia_vec, *d_prefix_U_dia_vec;
	h_prefix_U_dia_vec = (float*) calloc(rows+1, sizeof(float));
	cudaMalloc((void**)&d_prefix_U_dia_vec, sizeof(float) * (rows+1));
	cudaMemset(d_prefix_U_dia_vec,0, (rows+1)*sizeof(float));

	
	// ************************************ PARTITION BAND TO LU *********************************************
	float* d_lu;
	cudaMalloc((void**)&d_lu, sizeof(float) * (dia_count*d_band_mat.rows));
	cudaMemset(d_lu,0, sizeof(float) * (dia_count*d_band_mat.rows));

	find_dimensions_of_blocks_and_threads(h_band_mat.bandwidth, grids, blocks);
	dim3 grids_temp, blocks_temp;
	find_dimensions_of_blocks_and_threads(h_band_mat.bandwidth-1, grids_temp, blocks_temp);

	lu_decomp_timer.Start();
		
		// Perform the computation.
		for (int i = 0; i < h_band_mat.rows-1; ++i)
		{	
			compute_upper_triangle_row<<<grids,blocks>>>(i, h_band_mat.rows, h_band_mat.bandwidth, d_band_mat.data, d_lu);
			compute_lower_triangle_column<<<grids_temp,blocks_temp>>>(i, h_band_mat.rows, h_band_mat.bandwidth, d_band_mat.data, d_lu);
		}
		compute_upper_triangle_row<<<grids,blocks>>>(h_band_mat.rows-1, h_band_mat.rows, h_band_mat.bandwidth, d_band_mat.data, d_lu);
		cudaDeviceSynchronize();
	lu_decomp_timer.Stop();

	float* lu = (float*) calloc(dia_count*h_band_mat.rows, sizeof(float));
	cudaMemcpy(lu, d_lu, sizeof(float)*(dia_count*h_band_mat.rows), cudaMemcpyDeviceToHost);

	// Check whether the result is true.
	// print_host_band_matrix(h_band_mat);
	//naive_check_lu(lu, h_band_mat);

	band_matrix h_lu_mat;
	h_lu_mat.rows = h_band_mat.rows;
	h_lu_mat.cols = h_band_mat.cols;
	h_lu_mat.bandwidth = h_band_mat.bandwidth;
	h_lu_mat.nnz = (2*h_band_mat.bandwidth-1)* h_band_mat.rows - (h_band_mat.bandwidth-1)*h_band_mat.bandwidth;
	h_lu_mat.data = lu;

	// print_host_band_matrix(h_lu_mat); //For seeing the actual values of L and U

	// Preparing prefix multiplication of U's main diagonal
	cudaMemcpy(h_prefix_U_dia_vec, &d_lu[(bandwidth-1)*rows], rows*sizeof(float), cudaMemcpyDeviceToHost);

	float temp;
	float mul = 1;
	for (int i = 0; i < rows+1; ++i)
	{	
		temp = h_prefix_U_dia_vec[i];
		h_prefix_U_dia_vec[i] = mul;
		mul *= temp;
	}
	// print_host_float_array(h_prefix_U_dia_vec, rows+1);

	cudaMemcpy(d_prefix_U_dia_vec, h_prefix_U_dia_vec, (rows+1)*sizeof(float), cudaMemcpyHostToDevice);
	

	float* h_dets, *d_dets;
	h_dets = (float*) calloc(rows*rows, sizeof(float));

	// Set det(i,i) to value one
	for (int i = 0; i < rows; ++i)
			h_dets[i*rows+i] = 1;

	cudaMalloc((void**)&d_dets, sizeof(float) * rows * rows);
	cudaMemset(d_dets,0, sizeof(float) * rows * rows);
	
	upper_dets_timer.Start();
		//  Compute upper derivate matrix
		for (int dia_index = 0; dia_index < rows; ++dia_index)
		{
			find_dimensions_of_blocks_and_threads(rows-dia_index, grids, blocks);
			compute_upper_dets<<<grids,blocks>>>(rows, bandwidth, dia_count, dia_index , d_lu, d_dia_vec, d_dets);
			cudaDeviceSynchronize();
		}
	upper_dets_timer.Stop();

	lower_dets_timer.Start();
		// Compute lower derivate matrix
		for (int dia_index = 0; dia_index < rows; ++dia_index)
		{
			find_dimensions_of_blocks_and_threads(rows-dia_index, grids, blocks);
			compute_lower_dets<<<grids,blocks>>>(rows, bandwidth, dia_count, -1*dia_index , d_lu, d_dia_vec, d_dets);
			cudaDeviceSynchronize();
		}
	lower_dets_timer.Stop();
	// print_device_block_matrix(d_dets,rows);

	find_dimensions_of_blocks_and_threads(rows*rows, grids, blocks);
	inverse_timer.Start();
		find_inverse_of_L_and_U<<<grids,blocks>>>(rows, d_dets, d_prefix_U_dia_vec);
		cudaDeviceSynchronize();
	inverse_timer.Stop();

	cudaMemcpy(h_dets, d_dets, sizeof(float)*rows*rows, cudaMemcpyDeviceToHost);

	// Compute inv(L) and inv(U) as csr matrices  and find x
	csr_matrix h_U_inv_mat, h_L_inv_mat;
	csr_matrix d_U_inv_mat, d_L_inv_mat;

	float* h_Linvb, *d_Linvb;
	float* h_Ainvb, *d_Ainvb;

	h_Linvb = (float*) calloc(rows, sizeof(float));
	cudaMalloc((void**)&d_Linvb, sizeof(float) * rows);
	cudaMemset(d_Linvb,0, sizeof(float) * rows);

	h_Ainvb = (float*) calloc(rows, sizeof(float));
	cudaMalloc((void**)&d_Ainvb, sizeof(float) * rows);
	cudaMemset(d_Ainvb,0, sizeof(float) * rows);


	// Describing matrix trype for cusparse
	cusparseMatDescr_t cusparse_descrA = 0;
	cusparseCreateMatDescr(&cusparse_descrA);        
	cusparseSetMatType(cusparse_descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrA, CUSPARSE_INDEX_BASE_ZERO);
	float alpha = 1, beta=0;

	base_x_timer.Start();
		create_csr_mats_from_block_inverse(rows, h_dets, h_U_inv_mat, h_L_inv_mat, d_U_inv_mat, d_L_inv_mat);

		cusparseScsrmv(cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
						d_L_inv_mat.rows, d_L_inv_mat.cols, d_L_inv_mat.nnz, &alpha, cusparse_descrA,
						d_L_inv_mat.values, d_L_inv_mat.rowPtr, d_L_inv_mat.colIndex,
						d_b, &beta,
						d_Linvb);
		cudaDeviceSynchronize();
		

		cusparseScsrmv(cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
						d_U_inv_mat.rows, d_U_inv_mat.cols, d_U_inv_mat.nnz, &alpha, cusparse_descrA,
						d_U_inv_mat.values, d_U_inv_mat.rowPtr, d_U_inv_mat.colIndex,
						d_Linvb, &beta,
						d_Ainvb);
	base_x_timer.Stop();

	cudaMemcpy(h_Linvb, d_Linvb, rows*sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(h_Ainvb, d_Ainvb, rows*sizeof(float), cudaMemcpyDeviceToHost);

	// print_device_csr_matrix(d_U_inv_mat);
	// print_device_csr_matrix(d_L_inv_mat);

	// print_host_float_array(h_Linvb,rows);
	// print_host_float_array(h_Ainvb,rows);


	// *********************************** SOLVE Ly=b WHERE y=Ux ********************************
	
	// Allocate y on device and host
	float* h_y, *d_y;
	cudaMalloc((void**)&d_y, sizeof(float) * (d_band_mat.rows));
	cudaMemset(d_y,0, sizeof(float) * (d_band_mat.rows));

	h_y = (float*) calloc(d_band_mat.rows ,sizeof(float));

	find_dimensions_of_blocks_and_threads(d_band_mat.bandwidth-1, grids, blocks);
	lower_triag_timer.Start();
		for (int i = 0; i < d_band_mat.rows; ++i)
		{
			find_y<<<1,1>>>(d_b, d_y, i);
			update_y<<<grids,blocks>>>(d_lu, d_y, d_b, i, d_band_mat.bandwidth, d_band_mat.rows);
		}
		cudaDeviceSynchronize();
	lower_triag_timer.Stop();
	
	cudaMemcpy(h_y, d_y, d_band_mat.rows*sizeof(float), cudaMemcpyDeviceToHost);

	/*
	for (int i = 0; i < h_band_mat.rows; ++i)
		cout << h_y[i] << endl;
		cout << endl;
	*/

	// Allocate y on device and host
	float* h_x, *d_x;
	cudaMalloc((void**)&d_x, sizeof(float) * (d_band_mat.rows));
	cudaMemset(d_x,0, sizeof(float) * (d_band_mat.rows));

	h_x = (float*) calloc(d_band_mat.rows ,sizeof(float));
	
	find_dimensions_of_blocks_and_threads(d_band_mat.bandwidth-1, grids, blocks);
	upper_triag_timer.Start();
	for (int i = h_band_mat.rows-1; i >=0; --i)
	{
		/* code */
		find_x<<<1,1>>>(d_lu, d_y, d_x, i, d_band_mat.bandwidth, d_band_mat.rows);
		update_x<<<grids,blocks>>>(d_lu, d_x, d_y, i, d_band_mat.bandwidth, d_band_mat.rows);
	}
	cudaDeviceSynchronize();
	upper_triag_timer.Stop();

	cudaMemcpy(h_x, d_x, d_band_mat.rows*sizeof(float), cudaMemcpyDeviceToHost);

	naive_check_x(h_band_mat, h_x, h_b);
	naive_check_x(h_band_mat, h_Ainvb, h_b);

	// ******************************* ITERATIVE SOLUTION USING x AS THE BASIS POINT **************


	float* d_fin_x, *h_fin_x;
	h_fin_x = (float*) calloc(rows, sizeof(float));
	cudaMalloc((void**)&d_fin_x, sizeof(float) * (rows));
	cudaMemset(d_fin_x, 0, rows*sizeof(float));

	float tolerance = 0.1;
	int max_iters = 200;
	int iters = 0;

	spIterativeSolver(cusparse_handle, rows, tolerance, max_iters,
		d_qband_mat, d_b, d_dia_vec, h_x, d_fin_x, iters, sparse_correction_time);

	cout << "Band as Start Iter " << iters << endl;

	// print_device_float_array(d_fin_x, rows);

 	
    // **************** BENCHMARK ITERATIVE COMPUTATION ****************
    iters = 0;
	memset(h_x, 0, sizeof(float)*rows);
    spIterativeSolver(cusparse_handle, rows, tolerance, max_iters,
		d_qband_mat, d_b, d_dia_vec, h_x, d_fin_x, iters, benchmark_iter_time);

    cout << "Zero as Start Iter " << iters << endl;

    // print_device_float_array(d_fin_x, rows);


	// --------------- BENCHMARK CUSOLVER COMPUTATION ------------------
	/*
	float* h_bench_x, *d_bench_x;
	h_bench_x = (float*) calloc(h_band_mat.rows, sizeof(float));

	cudaMalloc((void**)&d_bench_x, sizeof(float) * (rows));
	cudaMemcpy(d_bench_x, h_bench_x, sizeof(float)*(rows), cudaMemcpyHostToDevice);

	// Create handle
	// cusolver handle
	cusolverSpHandle_t cusolver_handle;
	cusolverSpCreate(&cusolver_handle);

	

	benchmark_timer.Start();

		int singularity;
		cusolverStatus_t status = 
			cusolverSpScsrlsvqr(cusolver_handle, 
				d_qband_mat.rows,
				d_qband_mat.nnz, 
				cusparse_descrA, 
				d_qband_mat.values, 
				d_qband_mat.rowPtr,
				d_qband_mat.colIndex, 
				d_b, tolerance,0, d_bench_x, &singularity);

	benchmark_timer.Stop();

	cout << "Status "  << status << endl;


	cudaMemcpy(h_bench_x, d_bench_x, sizeof(float)*rows, cudaMemcpyDeviceToHost);
	*/

	/*
	for (int i = 0; i < rows; ++i)
	{
		cout << h_x[i] << " " << h_bench_x[i] << endl;
	}
	cout << endl;
	cusolverSpDestroy(cusolver_handle);
	*/
	cusparseDestroy(cusparse_handle);
	
	// *********************** TIMINGS ****************************

	lu_decomp_time = lu_decomp_timer.Elapsed();
	lower_triag_time = lower_triag_timer.Elapsed();
	upper_triag_time = upper_triag_timer.Elapsed();

	upper_dets_time = upper_dets_timer.Elapsed();
	lower_dets_time = lower_dets_timer.Elapsed();
	inverse_time = inverse_timer.Elapsed();
	base_x_time = base_x_timer.Elapsed();

	float no_inverse_time = lu_decomp_time + lower_triag_time + upper_triag_time + sparse_correction_time;
	float with_inverse_time = lu_decomp_time + upper_dets_time + lower_dets_time + inverse_time + base_x_time + sparse_correction_time;

	cout << "x from Band    Inv  " << with_inverse_time << "(" << (with_inverse_time - sparse_correction_time) << " + " << sparse_correction_time<< ")" <<endl;
	cout << "x from Band No-Inv  " << no_inverse_time << "(" << (no_inverse_time - sparse_correction_time) << " + " << sparse_correction_time<< ")" <<endl;
	cout << "x   as Zero " << benchmark_iter_time << endl;

	cout << lu_decomp_time << " " << lower_dets_time << " " << upper_dets_time << " " << inverse_time << " " << base_x_time << " " << sparse_correction_time << endl;
	cout << lu_decomp_time << " " << lower_triag_time << " " << upper_triag_time << " " << sparse_correction_time << endl;


	return 0;
}










