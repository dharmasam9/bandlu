
# Synthetic matrix properites
rows=10000
nnz_perc=0.5
band_perc=90

num_data_points=5
strip_size=0.2

result_dir="./results/"
hyphen="-"
mtype="qband"
result=$result_dir$mtype$hyphen$rows$hyphen$band_perc


if [ -f "$result" ]
then
	rm $result
else
	touch $result
fi


for ((  j = 0 ;  j < num_data_points;  j++  ))
	do
		./a.out "$rows" "$nnz_perc" "$band_perc"
		nnz_perc=$(echo "scale=2; $nnz_perc+$strip_size" | bc)
	done